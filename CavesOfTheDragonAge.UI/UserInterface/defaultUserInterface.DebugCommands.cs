﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    public partial class DefaultUserInterface
    {
        private void enterDebugCommand()
        {
            displayDijkestra = !displayDijkestra;
        }

        private bool displayDijkestra = false;
    }
}
