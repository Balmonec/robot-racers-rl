﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.MagicSpells;
using libtcod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    public partial class DefaultUserInterface
    {
        private CreatureAction castMagicSpell()
        {
            CreatureAction action = new CreatureAction();

            Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];

            action.ActionType = CreatureActionType.Cast;

            action.ActionMagnitude = getKnownSpell();

            if (action.ActionMagnitude == -1)
            {
                RenderAll();
                action = GetPlayerAction();
            }
            else
            {
                //action.ActionLocation = getTargetLocationForSpell(player.SpellsKnown[action.ActionMagnitude]);
                if(action.ActionLocation == new Location(-1, -1))
                {
                    RenderAll();
                    action = castMagicSpell();
                }
            }

            return action;
        }

        private int getKnownSpell()
        {
            RenderAll();

            Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];

            List<string> spellsKnown = new List<string>();

            //player.SpellsKnown.ForEach(x => spellsKnown.Add(x.Name));

            return DisplayMenu("Select a spell:", spellsKnown);
        }

        //private Location getTargetLocationForSpell(MagicSpell spell)
        //{
        //    switch(spell.Target.TargetType)
        //    {
        //        case TargetType.None:
        //        case TargetType.Self:
        //            return GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);

        //        case TargetType.Adjacent:
        //            {
        //                Direction direction = getDirectionFromUser();
        //                while (direction.IsHorizontalMovement() == false)
        //                {
        //                    direction = getDirectionFromUser();
        //                }
        //                return GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId).GetVector(direction);
        //            }

        //        case TargetType.LineOfSight:
        //            return getLOSLocationFromPlayer(spell, GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId));

        //        default:
        //            return GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);
        //    }
        //}

        //private Location getLOSLocationFromPlayer(MagicSpell spell, Location currentLocation)
        //{
        //    Location playerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);
        //    List<Location> spellLocations = spell.EffectList[0].AreaType.GetAreaLocations(currentLocation, spell.EffectList[0].AreaMagnitude, playerLocation);

        //    foreach(Location location in spellLocations)
        //    {
        //        if (GameData.DungeonMaps[GameData.CurrentDungeon].IsInDungeon(location))
        //        {
        //            GameData.DungeonMaps[GameData.CurrentDungeon].SFXLayer[location.X, location.Y] = new DisplayCharacter('x', System.Drawing.Color.Yellow);
        //        }
        //    }
        //    GameData.DungeonMaps[GameData.CurrentDungeon].SFXLayer[currentLocation.X, currentLocation.Y] = new DisplayCharacter('X', System.Drawing.Color.Green);

        //    RenderAll(true);

        //    GameData.DungeonMaps[GameData.CurrentDungeon].ClearSFX();

        //    TCODKey key = waitForKeypress(false);

        //    if (key.KeyCode == TCODKeyCode.Enter || key.KeyCode == TCODKeyCode.KeypadEnter)
        //    {
        //        return currentLocation;
        //    }
        //    else if(key.KeyCode == TCODKeyCode.Escape)
        //    {
        //        return new Location(-1, -1);
        //    }
        //    else if(key.KeyCode == TCODKeyCode.Tab)
        //    {
        //        //find location of closest visible creature
        //        int distance = int.MaxValue;
        //        Location newLocation = null;
        //        foreach(var creature in GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList)
        //        {
        //            if (creature.Key == "PLAYER")
        //            {
        //                continue;
        //            }

        //            Location creatureLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(creature.Key);
        //            if (GameData.DungeonMaps[GameData.CurrentDungeon].IsInDungeon(creatureLocation) &&
        //                GameData.DungeonMaps[GameData.CurrentDungeon].FOVState[creatureLocation.X, creatureLocation.Y] == Engine.Dungeons.Dungeon.FOVStateType.Visible)
        //            {
        //                if(GameData.DungeonMaps[GameData.CurrentDungeon].mapToPlayer.GoalMap(creatureLocation.X, creatureLocation.Y) < distance)
        //                {
        //                    distance = GameData.DungeonMaps[GameData.CurrentDungeon].mapToPlayer.GoalMap(creatureLocation.X, creatureLocation.Y);
        //                    newLocation = creatureLocation;
        //                }
        //            }
        //        }

        //        if(newLocation != null)
        //        {
        //            return getLOSLocationFromPlayer(spell, newLocation);
        //        }
        //        else
        //        {
        //            return getLOSLocationFromPlayer(spell, currentLocation);
        //        }
        //    }
        //    else
        //    {
        //        Direction moveDirection = interpretKeyDirection(key);
        //        Location newLocation = currentLocation.GetVector(moveDirection);
        //        if (GameData.DungeonMaps[GameData.CurrentDungeon].IsInDungeon(newLocation))
        //        {
        //            return getLOSLocationFromPlayer(spell, newLocation);
        //        }
        //        else
        //        {
        //            return getLOSLocationFromPlayer(spell, currentLocation);
        //        }
        //    }
        //}

        //private CreatureAction forgeMagicSpell()
        //{
        //    while (true)
        //    {
        //        int choice = DisplayMenu("Do you wish to:", new List<string>() { "Forge a new spell", "Edit an existing spell", "Cancel" });
        //        switch (choice)
        //        {
        //            case 0:
        //                forgeNewSpell();
        //                break;

        //            case 1:
        //                editExistingSpell();
        //                break;

        //            default:
        //                RenderAll();
        //                return GetPlayerAction();
        //        }
        //        RenderAll();
        //    }
        //}

        //private void forgeNewSpell()
        //{
        //    Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
        //    if (player.TargetRunesKnown.Count() == 0 || player.AreaRunesKnown.Count() == 0 || player.EffectRunesKnown.Count() == 0)
        //    {
        //        DisplayMessage("You do not know enough runes to forge a new spell.");
        //        DisplayMessage("You need to know at least one target rune, area rune, and effect rune.");
        //        DisplayMessage("You will learn more runes as you find more spells.");
        //        return;
        //    }

        //    string spellName = "";
        //    TargetRune targetRune = null;
        //    AreaRune areaRune = null;
        //    EffectRune effectRune = null;

        //    Tuple<bool, string, TargetRune, AreaRune, EffectRune> newRunes = selectRunes(spellName, targetRune, areaRune, effectRune, true);
            
        //    if(newRunes.Item1)
        //    {
        //        MagicSpell newMagicSpell = MagicSpellFactory.GetNewMagicSpell(newRunes.Item2, newRunes.Item3, newRunes.Item4, newRunes.Item5);

        //        player.Score += 15;
        //        player.LearnSpell(newMagicSpell);
        //    }
        //}

        //private void editExistingSpell()
        //{
        //    Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
        //    if (player.TargetRunesKnown.Count() == 0 && player.AreaRunesKnown.Count() == 0 && player.EffectRunesKnown.Count() == 0)
        //    {
        //        DisplayMessage("You do not know any runes to use to edit a spell.");
        //        DisplayMessage("You need to know at least one target rune, area rune, or effect rune.");
        //        DisplayMessage("You will learn more runes as you find more spells.");
        //        return;
        //    }

        //    int spellChoice = getKnownSpell();

        //    if(spellChoice == -1)
        //    {
        //        return;
        //    }

        //    string spellName = player.SpellsKnown[spellChoice].Name;
        //    TargetRune targetRune = player.SpellsKnown[spellChoice].TargetRune;
        //    AreaRune areaRune = player.SpellsKnown[spellChoice].AreaRune;
        //    EffectRune effectRune = player.SpellsKnown[spellChoice].EffectRune;

        //    Tuple<bool, string, TargetRune, AreaRune, EffectRune> newRunes = selectRunes(spellName, targetRune, areaRune, effectRune, false);

        //    if (newRunes.Item1)
        //    {
        //        MagicSpell newMagicSpell = MagicSpellFactory.GetNewMagicSpell(newRunes.Item2, newRunes.Item3, newRunes.Item4, newRunes.Item5);

        //        if (player.SpellsKnown[spellChoice] == newMagicSpell)
        //        {
        //            DisplayMessage("Spell Unchanged");
        //        }
        //        else
        //        {
        //            player.Score += 5;
        //            player.SpellsKnown[spellChoice] = newMagicSpell;
        //            DisplayMessage("Spell Updated");
        //        }
        //    }
        //}

        //private Tuple<bool, string, TargetRune, AreaRune, EffectRune> selectRunes(string spellName, TargetRune targetRune, AreaRune areaRune, EffectRune effectRune, bool newSpell)
        //{
        //    string newSpellName = spellName;
        //    TargetRune newTargetRune = new TargetRune(targetRune);
        //    AreaRune newAreaRune = new AreaRune(areaRune);
        //    EffectRune newEffectRune = new EffectRune(effectRune);

        //    Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
        //    bool targetRuneKnown = player.TargetRunesKnown.Any(rune => rune == targetRune);
        //    bool areaRuneKnown = player.AreaRunesKnown.Any(rune => rune == areaRune);
        //    bool effectRuneKnown = player.EffectRunesKnown.Any(rune => rune == effectRune);

        //    while (true)
        //    {
        //        RenderAll();
        //        int choice = DisplayMenu("Change Which Item:",
        //            new List<string>() { "Spell Name: \'"+newSpellName+"\'",
        //                                 "Target Rune: " + (targetRuneKnown || newSpell ? "\'" + newTargetRune.Name + "\'" : "Unknown Rune"),
        //                                 "Area Rune: " + (areaRuneKnown || newSpell ? "\'" + newAreaRune.Name + "\'" : "Unknown Rune"),
        //                                 "Effect Rune: " + (effectRuneKnown || newSpell ? "\'"+ newEffectRune.Name+ "\'" :"Unknown Rune"),
        //                                 "Finish Selection" });

        //        switch (choice)
        //        {
        //            case 0:
        //                {
        //                    RenderAll();
        //                    string temp = GetTextFromUser("Spell Name:", newSpellName);
        //                    if (temp != "")
        //                    {
        //                        newSpellName = temp;
        //                    }
        //                }
        //                break;

        //            case 1:
        //                if (targetRuneKnown || newSpell)
        //                {
        //                    newTargetRune = selectNewTargetRune(newTargetRune);
        //                }
        //                else
        //                {
        //                    int temp = DisplayMenu("You don't know that rune.", null);
        //                }
        //                break;

        //            case 2:
        //                if (areaRuneKnown || newSpell)
        //                {
        //                    newAreaRune = selectNewAreaRune(newAreaRune);
        //                }
        //                else
        //                {
        //                    int temp = DisplayMenu("You don't know that rune.", null);
        //                }
        //                break;

        //            case 3:
        //                if (effectRuneKnown || newSpell)
        //                {
        //                    newEffectRune = selectNewEffectRune(newEffectRune);
        //                }
        //                else
        //                {
        //                    int temp = DisplayMenu("You don't know that rune.", null);
        //                }
        //                break;

        //            case 4:
        //                {
        //                    if (newSpellName != "" && newTargetRune != null && newAreaRune != null && newEffectRune != null)
        //                    {
        //                        return new Tuple<bool, string, TargetRune, AreaRune, EffectRune>(true, newSpellName, newTargetRune, newAreaRune, newEffectRune);
        //                    }
        //                    else
        //                    {
        //                        RenderAll();
        //                        int temp = DisplayMenu("You must select all runes and a name to finish the spell!", null);
        //                        break;
        //                    }
        //                }

        //            case -1:
        //                return new Tuple<bool, string, TargetRune, AreaRune, EffectRune>(false, null, null, null, null);

        //            default:
        //                break;
        //        }
        //    }
        //}

        //private TargetRune selectNewTargetRune(TargetRune current)
        //{
        //    Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
        //    List<string> runes = new List<string>();

        //    foreach(TargetRune rune in player.TargetRunesKnown)
        //    {
        //        runes.Add("\'" + rune.Name + "\' - " + rune.Description);
        //    }

        //    RenderAll();
        //    int choice = DisplayMenu("Select a target rune:     (Current:\'" + current.Name + "\')", runes);
        //    if(choice == -1)
        //    {
        //        return current;
        //    }
        //    else
        //    {
        //        return player.TargetRunesKnown[choice];
        //    }
        //}

        //private AreaRune selectNewAreaRune(AreaRune current)
        //{
        //    Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
        //    List<string> runes = new List<string>();

        //    foreach (AreaRune rune in player.AreaRunesKnown)
        //    {
        //        runes.Add("\'" + rune.Name + "\' - " + rune.Description);
        //    }

        //    RenderAll();
        //    int choice = DisplayMenu("Select an area rune:     (Current:\'" + current.Name + "\')", runes);
        //    if (choice == -1)
        //    {
        //        return current;
        //    }
        //    else
        //    {
        //        return player.AreaRunesKnown[choice];
        //    }
        //}

        //private EffectRune selectNewEffectRune(EffectRune current)
        //{
        //    Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
        //    List<string> runes = new List<string>();

        //    foreach (EffectRune rune in player.EffectRunesKnown)
        //    {
        //        runes.Add("\'" + rune.Name + "\' - " + rune.Description);
        //    }

        //    RenderAll();
        //    int choice = DisplayMenu("Select an area rune:     (Current:\'" + current.Name + "\')", runes);
        //    if (choice == -1)
        //    {
        //        return current;
        //    }
        //    else
        //    {
        //        return player.EffectRunesKnown[choice];
        //    }
        //}
    }
}
