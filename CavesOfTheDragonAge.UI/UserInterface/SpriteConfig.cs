﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    public class SpriteConfig
    {
        public Dictionary<char, string> Sprites { get; set; }
    }
}
