﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine;
using CavesOfTheDragonAge.Engine.Creatures;
using libtcod;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    internal static class ConvertColors
    {
        public static TCODColor ToTCOD(this Color color)
        {
            return new TCODColor(color.R, color.G, color.B);
        }
    }

    partial class DefaultUserInterface : IUserInterface
    {
        //TODO: Handle infinite loops of messages, and allow the user to click X to exit.

        #region [-- Public Methods --]

        /// <summary>
        /// Displays a message in the default color of white.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void DisplayMessage(string message)
        {
            DisplayMessage(message, Color.White);
        }

        /// <summary>
        /// Displays a message in the given color.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="color">The color to the message in.</param>
        public void DisplayMessage(string message, Color color)
        {
            if (message.Length <= MessagePanel.getWidth())
            {
                //message is short enough to fit on the screen, add it to the
                //message buffer and message history.
                MessageList.Add(new Tuple<string, Color>(message, color));
                MessageHistory.Add(new Tuple<string, Color>(message, color));
            }
            else
            {
                //figure out a good place to break the message - somewhere there
                //is a space.
                int position = MessagePanel.getWidth() - 1;
                char currentCharacter = message[position];
                while (currentCharacter != ' ' && position > 0)
                {
                    currentCharacter = message[position];
                    position--;
                }
                if (position > 0)
                {
                    //a good breakpoint has been found, so break the message
                    //up and display both parts.
                    DisplayMessage(message.Substring(0, position + 1), color);
                    DisplayMessage(message.Substring(position + 1).TrimStart(), color);
                }
                else
                {
                    //no good breakpoint has been found, there are no spaces
                    //in the message before the width limit, so break it there.
                    DisplayMessage(message.Substring(0, MessagePanel.getWidth()), color);
                    DisplayMessage(message.Substring(MessagePanel.getWidth()), color);
                }
            }

            //if too many messages, display a set now
            if (MessageList.Count > MessagePanel.getHeight())
            {
                RenderAll();
                TCODConsole.waitForKeypress(true);
                for (int x = 0; x < MessagePanel.getHeight(); x++)
                {
                    MessageList.RemoveAt(0);
                }
            }
        }

        /// <summary>
        /// Initialize the user interface.
        /// </summary>
        /// <param name="width">The width of the user interface.</param>
        /// <param name="height">The height of the user interface.</param>
        /// <param name="title">The title of the user interface window.</param>
        public void InitUi(int width = 80, int height = 50, string title = "Caves of the Dragon Age")
        {
            try
            {
                TCODConsole.setCustomFont(loadSpritesheet(), (int)TCODFontFlags.LayoutAsciiInRow, 32, 312);

                TCODConsole.initRoot(width, height, title, false, TCODRendererType.SDL);

                //TCODConsole.mapAsciiCodesToFont(0, 65536, 0, 0);

                Con = new TCODConsole(width, height);
                MessagePanel = new TCODConsole(width, 5);
                SidebarPanel = new TCODConsole(15, (height - MessagePanel.getHeight()));
                InfoPanel = new TCODConsole(width - SidebarPanel.getWidth(), 7);
                DungeonPanel = new TCODConsole((width - SidebarPanel.getWidth()), (height - MessagePanel.getHeight() - InfoPanel.getHeight()));

                MessageList = new List<Tuple<string, Color>>();
                MessageHistory = new List<Tuple<string, Color>>();
            }
            catch(DllNotFoundException e)
            {
                if(e.Message == "Unable to load DLL 'libtcod-net-unmanaged': The specified module could not be found. (Exception from HRESULT: 0x8007007E)")
                {
                    var response = MessageBox.Show("Please Install Microsoft Visual C++ 2010 Service Pack 1." + Environment.NewLine +
                                                   Environment.NewLine +
                                                   "Would you like to open the download page?", "Dependency Required", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if(response == DialogResult.Yes) 
                    {
                        System.Diagnostics.Process.Start("https://www.microsoft.com/en-us/download/details.aspx?id=26999");
                    }
                }
                throw;
            }
        }

        /// <summary>
        /// render the entire screen.
        /// </summary>
        public void RenderAll(bool keepMessages = false, bool allowPause = false)
        {
            //Clear all the panels
            MessagePanel.clear();
            DungeonPanel.clear();
            SidebarPanel.clear();
            InfoPanel.clear();

            //display the current dungeon state.
            showDungeon();

            //load the messages onto the messagePanel
            bool allMessagesDisplayed = loadMessages();

            //show the sidebar panel
            showSidebar();

            //show the info panel
            showInfo();

            //blit subconsoles onto the main console
            TCODConsole.blit(MessagePanel, 0, 0, MessagePanel.getWidth(), MessagePanel.getHeight(), Con, 0, 0);
            TCODConsole.blit(DungeonPanel, 0, 0, DungeonPanel.getWidth(), DungeonPanel.getHeight(), Con, 0, MessagePanel.getHeight());
            TCODConsole.blit(SidebarPanel, 0, 0, SidebarPanel.getWidth(), SidebarPanel.getHeight(), Con, DungeonPanel.getWidth(), MessagePanel.getHeight());
            TCODConsole.blit(InfoPanel, 0, 0, InfoPanel.getWidth(), InfoPanel.getHeight(), Con, 0, MessagePanel.getHeight() + DungeonPanel.getHeight());

            // display on the main screen
            TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
            TCODConsole.flush();

            //if all messages not displayed, wait for the user to press a key
            if (allMessagesDisplayed == true && keepMessages == false)
            {
                MessageList.Clear();
            }

            //if pausable, check for the pause button
            if(allowPause == true)
            {
                var key = TCODConsole.checkForKeypress(1);
                if(key.Character == 'p')
                {
                    pressAKeyToContinue();

                    //blit subconsoles onto the main console
                    TCODConsole.blit(MessagePanel, 0, 0, MessagePanel.getWidth(), MessagePanel.getHeight(), Con, 0, 0);
                    TCODConsole.blit(DungeonPanel, 0, 0, DungeonPanel.getWidth(), DungeonPanel.getHeight(), Con, 0, MessagePanel.getHeight());
                    TCODConsole.blit(SidebarPanel, 0, 0, SidebarPanel.getWidth(), SidebarPanel.getHeight(), Con, DungeonPanel.getWidth(), MessagePanel.getHeight());
                    TCODConsole.blit(InfoPanel, 0, 0, InfoPanel.getWidth(), InfoPanel.getHeight(), Con, 0, MessagePanel.getHeight() + DungeonPanel.getHeight());

                    // display on the main screen
                    TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
                    TCODConsole.flush();

                    TCODConsole.waitForKeypress(true);
                }
            }
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Display the messages in the message panel.
        /// </summary>
        /// <returns>Returns true if all messages were displayed, and false if some remain to be displayed.</returns>
        private bool loadMessages()
        {
            MessagePanel.clear();

            int y = 0;
            foreach (Tuple<string, Color> mess in MessageList)
            {
                MessagePanel.setForegroundColor(mess.Item2.ToTCOD());
                MessagePanel.print(0, y, mess.Item1);
                y++;
                if (y > MessagePanel.getHeight())
                {
                    //not all messages displayed
                    pressAKeyToContinue();
                    return false;
                }
            }

            //all messages displayed
            return true;
        }

        /// <summary>
        /// Display the dungeon in the dungeon panel.
        /// </summary>
        private void showDungeon()
        {
            DungeonPanel.clear();

            Engine.Dungeons.Dungeon dungeon = GameData.DungeonMaps[GameData.CurrentDungeon];

            Location playerLocation = dungeon.GetCreatureLocation(PlayerId);

            #region Set Offsets

            int xOffset = playerLocation.X - DungeonPanel.getWidth() / 2;
            int yOffset = playerLocation.Y - DungeonPanel.getHeight() / 2;

            if (xOffset < 0)
            {
                xOffset = 0;
            }

            if (xOffset + DungeonPanel.getWidth() >= dungeon.Width)
            {
                xOffset = dungeon.Width - DungeonPanel.getWidth();
            }

            if(DungeonPanel.getWidth() >= dungeon.Width)
            {
                xOffset = dungeon.Width / 2 - DungeonPanel.getWidth() / 2;
            }

            if (yOffset < 0)
            {
                yOffset = 0;
            }

            if (yOffset + DungeonPanel.getHeight() >= dungeon.Height)
            {
                yOffset = dungeon.Height - DungeonPanel.getHeight();
            }

            if (DungeonPanel.getHeight() >= dungeon.Height)
            {
                yOffset = dungeon.Height / 2 - DungeonPanel.getHeight() / 2;
            }

            #endregion Set Offsets

            for (int y = 0; y < DungeonPanel.getHeight(); y++)
            {
                for (int x = 0; x < DungeonPanel.getWidth(); x++)
                {
                    DisplayCharacter display = dungeon.GetDisplayValue(xOffset + x, yOffset + y);
                    if (displayDijkestra == false)
                    {
                        DungeonPanel.putCharEx(x, y, display.Character, display.ForegroundColor.ToTCOD(), display.BackgroundColor.ToTCOD());
                    }
                    else
                    {
                        int mapValue = dungeon.MapToFlag.GoalMap(xOffset + x, yOffset + y);
                        mapValue = mapValue % 100;
                        mapValue = mapValue / 10;
                        DungeonPanel.putCharEx(x, y, mapValue.ToString()[0], display.ForegroundColor.ToTCOD(), display.BackgroundColor.ToTCOD());
                    }
                }
            }
        }

        private void showSidebar()
        {
            for(int x = 0; x < SidebarPanel.getWidth(); x++)
            {
                SidebarPanel.putCharEx(x, 0, '\u2550', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
                SidebarPanel.putCharEx(x, SidebarPanel.getHeight() - 1, '\u2550', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            }

            for (int y = 0; y < SidebarPanel.getHeight(); y++)
            {
                SidebarPanel.putCharEx(0, y, '\u2551', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
                SidebarPanel.putCharEx(SidebarPanel.getWidth() - 1, y, '\u2551', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            }

            SidebarPanel.putCharEx(0, 0, '\u2554', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            SidebarPanel.putCharEx(SidebarPanel.getWidth() - 1, 0, '\u2557', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            SidebarPanel.putCharEx(0, SidebarPanel.getHeight() - 1, '\u2569', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            SidebarPanel.putCharEx(SidebarPanel.getWidth() - 1, SidebarPanel.getHeight() - 1, '\u255d', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            SidebarPanel.putCharEx(0, SidebarPanel.getHeight() - InfoPanel.getHeight(), '\u2563', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
            SidebarPanel.putCharEx(0, SidebarPanel.getHeight() - InfoPanel.getHeight() + 2, '\u2563', Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());

            SidebarPanel.setForegroundColor(Color.DarkGray.ToTCOD());
            SidebarPanel.print(1, 1, "Commands:");
            
            for (int y = 0; y < (AvailableInstructions.Count < 10 ? AvailableInstructions.Count : 10); y++)
            {
                int yoffset = y + 2;

                SidebarPanel.setForegroundColor(instructionsRemaining(AvailableInstructions[y].Item1.Identifier) > 0 ? Color.Green.ToTCOD() : Color.DarkGray.ToTCOD());
                SidebarPanel.print(2, yoffset, (y + 1) % 10 + ":");
                for(int x = 0; x < AvailableInstructions[y].Item1.ListView.Length; x++)
                {
                    int xoffset = x + 5;
                    SidebarPanel.putCharEx(xoffset, yoffset,
                        AvailableInstructions[y].Item1.ListView[x].Character,
                        AvailableInstructions[y].Item1.ListView[x].ForegroundColor.ToTCOD(),
                        AvailableInstructions[y].Item1.ListView[x].BackgroundColor.ToTCOD());
                }

                SidebarPanel.setForegroundColor(Color.LightGray.ToTCOD());
                SidebarPanel.print(11, yoffset, "(" + instructionsRemaining(AvailableInstructions[y].Item1.Identifier) + ")");
            }

            int ypos = 12;

            SidebarPanel.setForegroundColor(Color.Green.ToTCOD());
            SidebarPanel.print(2, ypos, "Enter:");
            SidebarPanel.setForegroundColor(Color.Yellow.ToTCOD());
            SidebarPanel.print(8, ypos++, "Start");
            SidebarPanel.print(9, ypos++, "Turn");

            SidebarPanel.setForegroundColor(Color.Green.ToTCOD());
            SidebarPanel.print(2, ypos, "r:");
            SidebarPanel.setForegroundColor(Color.Yellow.ToTCOD());
            SidebarPanel.print(8, ypos++, "Reset");

            SidebarPanel.setForegroundColor(Color.Green.ToTCOD());
            SidebarPanel.print(2, ypos, "R:");
            SidebarPanel.setForegroundColor(Color.Yellow.ToTCOD());
            SidebarPanel.print(8, ypos++, "All");

            SidebarPanel.setForegroundColor(Color.Green.ToTCOD());
            SidebarPanel.print(2, ypos, "p:");
            SidebarPanel.setForegroundColor(Color.Yellow.ToTCOD());
            SidebarPanel.print(8, ypos++, "Pause");

            SidebarPanel.setForegroundColor(Color.Green.ToTCOD());
            SidebarPanel.print(2, ypos, "Esc:");
            SidebarPanel.setForegroundColor(Color.Yellow.ToTCOD());
            SidebarPanel.print(8, ypos++, "Quit");
        }

        /// <summary>
        /// Show the information in the info panel.
        /// </summary>
        private void showInfo()
        {
            char[,] panelOutline = new char[,]
            {
                { '\u2554', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2563' },
                { '\u2551',    ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',   '\u2551' },
                { '\u2560', '\u2550', '\u21d3', '\u2550', '\u2566', '\u2550', '\u21d3', '\u2550', '\u2566', '\u2550', '\u21d3', '\u2550', '\u2566', '\u2550', '\u21d3', '\u2550', '\u2566', '\u2550', '\u21d3', '\u2550', '\u2566', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2563' },
                { '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',   '\u2551' },
                { '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',   '\u2551' },
                { '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',   '\u2551',    ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',      ' ',   '\u2551' },
                { '\u255a', '\u2550', '\u2550', '\u2550', '\u2569', '\u2550', '\u2550', '\u2550', '\u2569', '\u2550', '\u2550', '\u2550', '\u2569', '\u2550', '\u2550', '\u2550', '\u2569', '\u2550', '\u2550', '\u2550', '\u2569', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2550', '\u2569' },
            };

            for (int y = 0; y < panelOutline.GetLength(0); y++)
            {
                for (int x = 0; x < panelOutline.GetLength(1); x++)
                {
                    InfoPanel.putCharEx(x, y, panelOutline[y,x], Color.DarkGray.ToTCOD(), Color.Black.ToTCOD());
                }
            }

            char[,] queueHighlight = new char[,]
            {
                { '\u250f', '\u2501', '\u21d3', '\u2501', '\u2513' },
                { '\u2503',    ' ',      ' ',      ' ',   '\u2503' },
                { '\u2503',    ' ',      ' ',      ' ',   '\u2503' },
                { '\u2503',    ' ',      ' ',      ' ',   '\u2503' },
                { '\u2517', '\u2501', '\u2501', '\u2501', '\u251b' }
            };

            for (int y = 0; y < queueHighlight.GetLength(0); y++)
            {
                for (int x = 0; x < queueHighlight.GetLength(1); x++)
                {
                    InfoPanel.putCharEx(x + 4 * SelectedInstruction, y + 2, queueHighlight[y, x], Color.Yellow.ToTCOD(), Color.Black.ToTCOD());
                }
            }

            for (int i = 0; i < MaxInstructions; i++)
            {
                InfoPanel.putCharEx(4 * i + 2, 1,
                    QueuedInstructionKeys[i],
                    QueuedInstructions[i] == null || SelectedInstruction == i ? Color.Yellow.ToTCOD() : Color.Green.ToTCOD(),
                    Color.Black.ToTCOD());

                if (QueuedInstructions[i] != null)
                {
                    int xoffset = i * 4 + 1;
                    int yoffset = 3;
                    for (int x = 0; x < 3; x++)
                    {
                        for (int y = 0; y < 3; y++)
                        {
                            InfoPanel.putCharEx(x + xoffset, y + yoffset,
                                QueuedInstructions[i].CardView[y, x].Character,
                                QueuedInstructions[i].CardView[y, x].ForegroundColor.ToTCOD(),
                                QueuedInstructions[i].CardView[y, x].BackgroundColor.ToTCOD());
                        }
                    }
                }
            }

            //InfoPanel.putCharEx(2, 1, 'a', QueuedInstructions[0] == null ? Color.Yellow.ToTCOD() : Color.Green.ToTCOD(), Color.Black.ToTCOD());
            //InfoPanel.putCharEx(6, 1, 's', QueuedInstructions[1] == null ? Color.Yellow.ToTCOD() : Color.Green.ToTCOD(), Color.Black.ToTCOD());
            //InfoPanel.putCharEx(10, 1, 'd', QueuedInstructions[2] == null ? Color.Yellow.ToTCOD() : Color.Green.ToTCOD(), Color.Black.ToTCOD());
            //InfoPanel.putCharEx(14, 1, 'f', QueuedInstructions[3] == null ? Color.Yellow.ToTCOD() : Color.Green.ToTCOD(), Color.Black.ToTCOD());
            //InfoPanel.putCharEx(18, 1, 'g', QueuedInstructions[4] == null ? Color.Yellow.ToTCOD() : Color.Green.ToTCOD(), Color.Black.ToTCOD());

            Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];

            for (int x = 0; x < (player.Health * 4); x++)
            {
                InfoPanel.setCharBackground(x + 1, 1, Color.DarkRed.ToTCOD());
            }

            //InfoPanel.setForegroundColor(ConvertColors.ToTCOD(GameColors.Red));
            //InfoPanel.print(1, 0, "Health: " + player.Health.Current + "/" + player.Health.Maximum);

            //InfoPanel.setForegroundColor(ConvertColors.ToTCOD(Color.White));
            //InfoPanel.print(1, 1, "Depth : " + GameData.Depth + "/" + GameData.MaximumDepth);

            //InfoPanel.setForegroundColor(player.Score >= 0 ? ConvertColors.ToTCOD(Color.Green) : ConvertColors.ToTCOD(GameColors.Red));
            //InfoPanel.print(20, 0, "Score: " + player.Score);
        }

        private void loadPlayerPreview()
        {
            var previewLocations = GameData.DungeonMaps[GameData.CurrentDungeon].ForcastMovements(
                GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId),
                GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId].Facing,
                QueuedInstructions);

            foreach(var preview in previewLocations)
            {
                if (GameData.DungeonMaps[GameData.CurrentDungeon].IsInDungeon(preview.Item1))
                {
                    char previewChar;
                    switch(preview.Item2)
                    {
                        case Direction.North:
                            previewChar = '\u2191';
                            break;
                        case Direction.South:
                            previewChar = '\u2193';
                            break;
                        case Direction.East:
                            previewChar = '\u2192';
                            break;
                        case Direction.West:
                            previewChar = '\u2190';
                            break;
                        default:
                            previewChar = 'x';
                            break;
                    }
                    GameData.DungeonMaps[GameData.CurrentDungeon].SFXLayer[preview.Item1.X, preview.Item1.Y] = new DisplayCharacter(previewChar, Color.Yellow);
                }
            }

            if (GameData.DungeonMaps[GameData.CurrentDungeon].GetTile(previewLocations.Last().Item1).Attributes.Contains("Deadly"))
            {
                GameData.DungeonMaps[GameData.CurrentDungeon].SFXLayer[previewLocations.Last().Item1.X, previewLocations.Last().Item1.Y].BackgroundColor = Color.DarkRed;
            }
            else
            {
                GameData.DungeonMaps[GameData.CurrentDungeon].SFXLayer[previewLocations.Last().Item1.X, previewLocations.Last().Item1.Y].BackgroundColor = Color.DarkGreen;
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        public TCODConsole Con { get; set; }

        public TCODConsole DungeonPanel { get; set; }

        public TCODConsole InfoPanel { get; set; }

        public TCODConsole MessagePanel { get; set; }

        public TCODConsole SidebarPanel { get; set; }

        public List<Tuple<string, Color>> MessageHistory { get; set; }

        public List<Tuple<string, Color>> MessageList { get; set; }

        public Instruction[] QueuedInstructions { get; set; } = new Instruction[MaxInstructions];
        public const int MaxInstructions = 5;

        public int SelectedInstruction { get; set; } = 0;

        public static List<char> QueuedInstructionKeys { get; set; } = new List<char>() { 'a', 'b', 'c', 'd', 'e' };

        public List<Tuple<Instruction, int>> AvailableInstructions = new List<Tuple<Instruction, int>>();

        #endregion [-- Properties --]
    }
}