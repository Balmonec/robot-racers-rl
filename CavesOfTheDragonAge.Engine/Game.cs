using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.Creatures.AI;
using CavesOfTheDragonAge.Engine.Dungeons;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CavesOfTheDragonAge.Engine
{
    /// <summary>
    /// Game
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Game
        /// </summary>
        /// <param name="userInterface"></param>
        public Game(IUserInterface userInterface)
        {
            GameData.Setup(userInterface);
        }

        public void MainMenu()
        {
            var options = new List<string>() {
                                    "New Game",
                                    "Credits",
                                    "Exit" };

            int choice = GameData.Ui.DisplayMenu("Main Menu", options,
                                    false,
                                    0,
                                    "mainmenu.png");

            switch(choice)
            {
                case 0:
                    while (NewGame())
                    {
                        Run();
                    }
                    MainMenu();
                    break;

                case 1:
                    GameData.Ui.DisplayMenu(File.ReadAllText("Credits.txt"), null, false, 0, "mainmenu.png");
                    MainMenu();
                    break;

                case 2:
                    break;

                default:
                    MainMenu();
                    break;
            }
        }

        public bool NewGame()
        {
            var availableLevels = DungeonFactory.GetAvailableLevels();

            int level = GameData.Ui.DisplayMenu("Please choose a level:", availableLevels.Select(l => l.Item1).ToList(), false, 0, "mainmenu.png");

            if(level < 0)
            {
                return false;
            }

            if (!string.IsNullOrEmpty(availableLevels[level].Item2.GetStartingText()))
            {
                GameData.Ui.DisplayMenu(availableLevels[level].Item2.GetStartingText(), null, false, 0, "mainmenu.png");
            }

            GameData.CurrentFormat = availableLevels[level].Item2;

            SetupGame(availableLevels[level].Item2);

            return true;
        }

        public void StartOver()
        {
            SetupGame(GameData.CurrentFormat);

            Run();
        }

        public void SetupGame(DungeonFormat level)
        {
            GameData.DungeonMaps = new Dictionary<string, Dungeon>
            {
                { "DUNGEONLEVEL0", DungeonFactory.ByJsonFile(level) }
            };

            GameData.CurrentDungeon = "DUNGEONLEVEL0";
            GameData.EndGame = false;
            GameData.StartOver = false;

            var creatureList = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList.Values.Where(c => c.Mobile == true).ToList();
            Creature creature;

            if (creatureList.Count > 1)
            {
                int choice = GameData.Ui.DisplayMenu("Which robot would you like to select?",
                                        creatureList.Select(c => c.Name).ToList(),
                                        false,
                                        0,
                                        "mainmenu.png");

                if (choice > 0)
                {
                    creature = creatureList[choice];
                }
                else
                {
                    creature = creatureList[0];
                }
            }
            else
            {
                creature = creatureList[0];
            }

            creature.Ai = new PlayerAi();
            creature.Ai.MyCreature = creature;
            GameData.Ui.PlayerId = creature.Identifier;
        }

        /// <summary>
        /// Run
        /// </summary>
        public void Run()
        {
            //GameData.Ui.DisplayMessage("Press '?' for a help menu.", System.Drawing.Color.Yellow);

            GameData.Ui.RenderAll();

            //TODO: Find a way to fix the isWindowClosed hack.
            while (!GameData.EndGame /*&& !TCODConsole.isWindowClosed()*/)
            {
                GameData.GoDown = false;
                GameData.StartOver = false;

                foreach (KeyValuePair<string, Creature> creaturePair in GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList)
                {
                    if (creaturePair.Value.Mobile == true)
                    {
                        creaturePair.Value.LoadActions();
                        if (GameData.EndGame == true)
                        {
                            break;
                        }
                    }
                }

                if (GameData.EndGame == false)
                {
                    for(int register = 0; register < 5; register++)
                    {
                        GameData.Ui.SelectedInstruction = register;

                        var creatures = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList.Values.Where(c => c.Mobile == true).ToList()
                            .OrderByDescending(creature => creature.QueuedInstructions[register].Priority);

                        //Move robots
                        foreach(var creature in creatures)
                        {
                            if (creature.Health > 0 && creature.Mobile == true)
                            {
                                GameData.DungeonMaps[GameData.CurrentDungeon].ExecuteInstruction(creature.Identifier, register);

                                if (GameData.EndGame == true)
                                {
                                    break;
                                }

                                GameData.Ui.RenderAll(true, true);
                            }
                        }

                        if (GameData.StartOver == true)
                        {
                            StartOver();
                            break;
                        }

                        //Move fast conveyor belts
                        GameData.DungeonMaps[GameData.CurrentDungeon].RunConveyors("FastPush");

                        if (GameData.StartOver == true)
                        {
                            StartOver();
                            break;
                        }

                        if (GameData.EndGame == true)
                        {
                            break;
                        }

                        //Move conveyor belts
                        GameData.DungeonMaps[GameData.CurrentDungeon].RunConveyors("Push");

                        if (GameData.StartOver == true)
                        {
                            StartOver();
                            break;
                        }

                        if (GameData.EndGame == true)
                        {
                            break;
                        }

                        GameData.DungeonMaps[GameData.CurrentDungeon].Rotate();

                        GameData.DungeonMaps[GameData.CurrentDungeon].CheckWinCondition();

                        if (GameData.StartOver == true)
                        {
                            StartOver();
                            break;
                        }

                        if (GameData.EndGame == true)
                        {
                            break;
                        }

                        //if (GameData.GoDown == true)
                        //{
                        //    NextLevel();
                        //    break;
                        //}
                    }

                    //GameData.DungeonMaps[GameData.CurrentDungeon].UpdateCells();
                }
            }
        }

        //private string getScoreMessage(int score)
        //{
        //    if(score < 25)
        //    {
        //        return "Final Score: " + score + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You have been given a full ride scholarship..... at the Warrior and" + System.Environment.NewLine
        //             + "Swordfighter Academy across town." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "Please do not apply to the Wizard Academy again." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You win?";
        //    }
        //    else if (score < 100)
        //    {
        //        return "Final Score: " + score + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You must have run away from every monster you came across to get such" + System.Environment.NewLine
        //             + "a poor score.  Either you are a coward (have you considered being a" + System.Environment.NewLine
        //             + "peasant), or you are very sneaky (perhaps you should consider a career" + System.Environment.NewLine
        //             + "as a spy?)." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You managed not to die, so.. you win?";
        //    }
        //    else if(score < 300)
        //    {
        //        return "Final Score: " + score + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You are grudgingly accepted into the Wizard Academy.  If you work hard" + System.Environment.NewLine
        //             + "and live frugally, you might even be able to pay off your student debt" + System.Environment.NewLine
        //             + "in fifty to a hundred years." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You win.";
        //    }
        //    else if(score < 500)
        //    {
        //        return "Final Score: " + score + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You are accepted into the Wizard Academy, and given the standard" + System.Environment.NewLine
        //             + "scholarships.  A (hopefully) profitable career as a Wizard awaits" + System.Environment.NewLine
        //             + "you!  If you can do well on all your classes, that is..." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You win.";
        //    }
        //    else if (score < 600)
        //    {
        //        return "Final Score: " + score + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You are gladly accepted into the Wizard Academy, and invited to join" + System.Environment.NewLine
        //             + "the honors club with a substantial scholarship package waiting for" + System.Environment.NewLine
        //             + "you.  We look forward to seeing how you will benefit our institution." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You win.";
        //    }
        //    else
        //    {
        //        return "Final Score: " + score + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You passed with flying colours, and are given a full ride scholarship!" + System.Environment.NewLine
        //             + "A long and profitable career as a Wizard is in your future!  If you" + System.Environment.NewLine
        //             + "continue to show this much promise, you could be on the fast track to be" + System.Environment.NewLine
        //             + "High Magister of the College, or at the very least an extraordinarily" + System.Environment.NewLine
        //             + "generous alumnus." + System.Environment.NewLine
        //             + System.Environment.NewLine
        //             + "You Win!";
        //    }
        //}

        //public void NextLevel()
        //{
        //    int choice = GameData.Ui.DisplayMenu("Do you wish to descend to the next level?", new List<string>() { "Yes", "No" });

        //    if (choice == 0)
        //    {
        //        //advance to the next level
        //        Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList["PLAYER"];

        //        GameData.Ui.DisplayMessage("You take a moment to rest, and recover your strength.", GameColors.LightBlue);
        //        player.Health = player.Health + (player.Health.Maximum / 2);

        //        GameData.Ui.DisplayMessage("After a rare moment of peace, you descend deeper into the heart of the dungeon...", GameColors.Red);

        //        Location oldPlayerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(player.Identifier);

        //        string newLevelName = "DUNGEONLEVEL" + GameData.DungeonMaps.Count;
        //        GameData.DungeonMaps.Add(newLevelName, DungeonFactory.GetDungeon(DungeonFactory.GeneratorType.ManualTesting, newLevelName, new System.Tuple<int, int>(80, 40), player));
        //        GameData.CurrentDungeon = newLevelName;
        //    }
        //}
    }
}