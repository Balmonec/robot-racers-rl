﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.MagicSpells
{
    public static partial class MagicRuneFactory
    {
        /// <summary>
        /// Get a new TargetRune.
        /// </summary>
        /// <param name="type">The type of rune to generate.</param>
        /// <returns></returns>
        public static TargetRune GetNewTargetRune(TargetType type)
        {
            switch (type)
            {
                //OK
                case TargetType.Self:
                    return new TargetRune()
                    {
                        Name = "Target Self",
                        Description = "Target the spell at the caster's position.",
                        SpellTarget = new SpellTarget() { TargetType = TargetType.Self, Magnitude = 0 }
                    };
                    //OK
                case TargetType.Adjacent:
                    return new TargetRune()
                    {
                        Name = "Target Adjacent",
                        Description = "Target the spell at a position adjacent to the caster.",
                        SpellTarget = new SpellTarget() { TargetType = TargetType.Adjacent, Magnitude = 0 }
                    };
                    //NEED
                case TargetType.LineOfSight:
                    return new TargetRune()
                    {
                        Name = "Target In View",
                        Description = "Target the spell at a position the caster can see.",
                        SpellTarget = new SpellTarget() { TargetType = TargetType.LineOfSight, Magnitude = 0 }
                    };

                case TargetType.None:
                default:
                    return new TargetRune()
                    {
                        Name = "No Targeting",
                        Description = "No targeting method given.",
                        SpellTarget = new SpellTarget() { TargetType = TargetType.Adjacent, Magnitude = 0 }
                    };
            }
        }

        /// <summary>
        /// Get a new AreaRune.
        /// </summary>
        /// <param name="type">The type of rune to generate.</param>
        /// <returns></returns>
        public static AreaRune GetNewAreaRune(string type)
        {
            switch (type)
            {

                case "SINGLE":
                    return new AreaRune()
                    {
                        Name = "Single",
                        Description = "Affect a single square.",
                        AreaType = AreaType.Single,
                        AreaMagnitude = 0
                    };

                case "SMALLCONE":
                    return new AreaRune()
                    {
                        Name = "Small Cone",
                        Description = "Affect a small cone pointing towards the target from the caster.",
                        AreaType = AreaType.Cone,
                        AreaMagnitude = 2
                    };

                case "MEDIUMCONE":
                    return new AreaRune()
                    {
                        Name = "Medium Cone",
                        Description = "Affect a medium cone pointing towards the target from the caster.",
                        AreaType = AreaType.Cone,
                        AreaMagnitude = 4
                    };

                case "LARGECONE":
                    return new AreaRune()
                    {
                        Name = "Large Cone",
                        Description = "Affect a large cone pointing towards the target from the caster.",
                        AreaType = AreaType.Cone,
                        AreaMagnitude = 6
                    };

                case "HUGECONE":
                    return new AreaRune()
                    {
                        Name = "Huge Cone",
                        Description = "Affect a huge cone pointing towards the target from the caster.",
                        AreaType = AreaType.Cone,
                        AreaMagnitude = 10
                    };

                case "SMALLEXPLOSION":
                    return new AreaRune()
                    {
                        Name = "Small Explosion",
                        Description = "Affect a small circular area around the target.",
                        AreaType = AreaType.Explosion,
                        AreaMagnitude = 1
                    };

                case "MEDIUMEXPLOSION":
                    return new AreaRune()
                    {
                        Name = "Medium Explosion",
                        Description = "Affect a medium circular area around the target.",
                        AreaType = AreaType.Explosion,
                        AreaMagnitude = 3
                    };

                case "LARGEEXPLOSION":
                    return new AreaRune()
                    {
                        Name = "Large Explosion",
                        Description = "Affect a large circular area around the target.",
                        AreaType = AreaType.Explosion,
                        AreaMagnitude = 5
                    };

                case "HUGEEXPLOSION":
                    return new AreaRune()
                    {
                        Name = "Huge Explosion",
                        Description = "Affect a huge circular area around the target.",
                        AreaType = AreaType.Explosion,
                        AreaMagnitude = 10
                    };

                case "SHORTLINE":
                    return new AreaRune()
                    {
                        Name = "Short Line",
                        Description = "Affect in a short line pointing towards the target from the caster.",
                        AreaType = AreaType.Line,
                        AreaMagnitude = 3
                    };

                case "MEDIUMLINE":
                    return new AreaRune()
                    {
                        Name = "Medium Line",
                        Description = "Affect in a medium line pointing towards the target from the caster.",
                        AreaType = AreaType.Line,
                        AreaMagnitude = 6
                    };

                case "LONGLINE":
                    return new AreaRune()
                    {
                        Name = "Long Line",
                        Description = "Affect in a long line pointing towards the target from the caster.",
                        AreaType = AreaType.Line,
                        AreaMagnitude = 9
                    };

                case "LEVEL":
                    return new AreaRune()
                    {
                        Name = "Entire Dungeon",
                        Description = "Affect the entire dungeon.",
                        AreaType = AreaType.Level,
                        AreaMagnitude = 0
                    };

                default:
                    return new AreaRune()
                    {
                        Name = "No Area",
                        Description = "No area of affect given.",
                        AreaType = AreaType.None,
                        AreaMagnitude = 0
                    };
            }
        }

        /// <summary>
        /// Get a new EffectRune.
        /// </summary>
        /// <param name="type">The type of rune to generate.</param>
        /// <returns></returns>
        public static EffectRune GetNewEffectRune(string type)
        {
            switch (type)
            {
                //OK
                case "HEAL":
                    return new EffectRune()
                    {
                        Name = "Healing",
                        Description = "Heal 5 hit points.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Heal,
                                EffectMagnitude = 5,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('#', GameColors.Azure),
                                EffectDescription = "{0} heals {1} hit points."
                            }
                        }
                    };

                    //OK
                case "FIRE":
                    return new EffectRune()
                    {
                        Name = "Fire",
                        Description = "Burn for 5 hit points of damage.  The heat will start waves in bodies of water.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Damage,
                                EffectMagnitude = 5,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('*', GameColors.Red),
                                EffectDescription = "{0} is burned for {1} hit points."
                            },
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "BURN",
                                EffectAppearance = null,
                                EffectDescription = ""
                            }
                        }
                    };

                case "ICE":
                    return new EffectRune()
                    {
                        Name = "Ice",
                        Description = "Freeze for 2 hit points of damage.  The cold will freeze over bodies of water.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Damage,
                                EffectMagnitude = 2,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('*', System.Drawing.Color.White),
                                EffectDescription = "{0} is frozen for {1} hit points."
                            },
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "FREEZE",
                                EffectAppearance = null,
                                EffectDescription = ""
                            }
                        }
                    };

                case "FROSTBITE":
                    return new EffectRune()
                    {
                        Name = "Frostbite",
                        Description = "Freeze for 6 hit points of damage.  The cold will freeze over bodies of water.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Damage,
                                EffectMagnitude = 6,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('*', System.Drawing.Color.White),
                                EffectDescription = "{0} is frozen for {1} hit points."
                            },
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "FREEZE",
                                EffectAppearance = null,
                                EffectDescription = ""
                            }
                        }
                    };

                case "FORCE":
                    return new EffectRune()
                    {
                        Name = "Force",
                        Description = "Batter for 7 hit points of damage.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Damage,
                                EffectMagnitude = 7,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('*', System.Drawing.Color.White),
                                EffectDescription = "{0} is battered for {1} hit points."
                            },
                        }
                    };

                case "LIGHTNING":
                    return new EffectRune()
                    {
                        Name = "Lightning",
                        Description = "Shock for 5 hit points of damage.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Damage,
                                EffectMagnitude = 5,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('~', GameColors.Azure),
                                EffectDescription = "{0} is shocked for {1} hit points."
                            },
                        }
                    };

                case "EARTH":
                    return new EffectRune()
                    {
                        Name = "Earth",
                        Description = "Crush for 5 hit points of damage.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Damage,
                                EffectMagnitude = 5,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter('#', GameColors.Brown),
                                EffectDescription = "{0} is crushed for {1} hit points."
                            },
                        }
                    };

                    //OK
                case "TELEPORT":
                    return new EffectRune()
                    {
                        Name = "Teleport",
                        Description = "Teleport the caster.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Teleport,
                                EffectMagnitude = 0,
                                EffectTag = "Destination Blocked",
                                EffectAppearance = new DisplayCharacter('#', System.Drawing.Color.Purple),
                                EffectDescription = "{0} teleports in a cloud of smoke!"
                            }
                        }
                    };

                case "PRY":
                    return new EffectRune()
                    {
                        Name = "Pry",
                        Description = "Pry something open.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "OPEN",
                                EffectAppearance = new DisplayCharacter('#', System.Drawing.Color.WhiteSmoke),
                                EffectDescription = "Everything in the area flies open!"
                            },
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "HANDLE",
                                EffectAppearance = null,
                                EffectDescription = ""
                            }
                        }
                    };

                case "SLAM":
                    return new EffectRune()
                    {
                        Name = "Slam",
                        Description = "Slam something closed.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "CLOSE",
                                EffectAppearance = new DisplayCharacter('#', System.Drawing.Color.WhiteSmoke),
                                EffectDescription = "Everything in the area is slammed shut!"
                            },
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "HANDLE",
                                EffectAppearance = null,
                                EffectDescription = ""
                            }
                        }
                    };

                case "DIG":
                    return new EffectRune()
                    {
                        Name = "Dig",
                        Description = "Dig through a wall.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.Trigger,
                                EffectMagnitude = 0,
                                EffectTag = "DIG",
                                EffectAppearance = new DisplayCharacter('!', GameColors.SaddleBrown),
                                EffectDescription = "The rock crumbles away!"
                            },
                        }
                    };

                default:
                    return new EffectRune()
                    {
                        Name = "No Effect",
                        Description = "No effect given.",
                        Effects = new List<SpellEffect>()
                        {
                            new SpellEffect
                            {
                                EffectType = EffectType.None,
                                EffectMagnitude = 0,
                                EffectTag = "",
                                EffectAppearance = new DisplayCharacter(),
                                EffectDescription = ""
                            }
                        }
                    };
            }
        }
    }
}
