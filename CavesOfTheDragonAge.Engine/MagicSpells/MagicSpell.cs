﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.MagicSpells
{
    public class MagicSpell
    {
        #region [-- Constructors --]

        public MagicSpell(string name)
        {
            Name = name;
            Target = new SpellTarget();
            EffectList = new List<SpellEffect>();
            TargetRune = new TargetRune();
            AreaRune = new AreaRune();
            EffectRune = new EffectRune();
        }

        public MagicSpell(MagicSpell magicSpell)
        {
            if (magicSpell != null)
            {
                Name = magicSpell.Name;
                Target = new SpellTarget(magicSpell.Target);
                EffectList = new List<SpellEffect>(magicSpell.EffectList);
                TargetRune = new TargetRune(magicSpell.TargetRune);
                AreaRune = new AreaRune(magicSpell.AreaRune);
                EffectRune = new EffectRune(magicSpell.EffectRune);
            }
        }

        #endregion [-- Constructors --]

        #region [-- Public Methods --]

        public static bool operator ==(MagicSpell left, MagicSpell right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.Name == right.Name && left.Target == right.Target && left.EffectRune == right.EffectRune && left.EffectList.All(effectLeft => right.EffectList.Any(effectRight => effectLeft == effectRight))
                && left.TargetRune == right.TargetRune && left.AreaRune == right.AreaRune ;
        }

        public static bool operator !=(MagicSpell left, MagicSpell right)
        {
            return !(left == right);
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The name of the spell.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The method to use to target the spell.
        /// </summary>
        public SpellTarget Target { get; set; }

        /// <summary>
        /// An ordered list of the areas and effects of the spell.
        /// </summary>
        public List<SpellEffect> EffectList;

        public TargetRune TargetRune { get; set; }

        public AreaRune AreaRune { get; set; }

        public EffectRune EffectRune { get; set; }

        #endregion [-- Properties --]

        #region [-- Enumerations --]

        #endregion [-- Enumerations --]
    }
}
