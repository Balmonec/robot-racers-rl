﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.MagicSpells
{
    public class TargetRune
    {
        public TargetRune()
        { }

        public TargetRune(TargetRune targetRune)
        {
            if (targetRune != null)
            {
                Name = targetRune.Name;
                Description = targetRune.Description;
                SpellTarget = new SpellTarget(targetRune.SpellTarget);
            }
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public SpellTarget SpellTarget;

        public static bool operator ==(TargetRune left, TargetRune right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.Name == right.Name && left.Description == right.Description && left.SpellTarget == right.SpellTarget;
        }

        public static bool operator !=(TargetRune left, TargetRune right)
        {
            return !(left == right);
        }
    }

    public class AreaRune
    {
        public AreaRune()
        { }

        public AreaRune(AreaRune areaRune)
        {
            if (areaRune != null)
            {
                Name = areaRune.Name;
                Description = areaRune.Description;
                AreaType = areaRune.AreaType;
                AreaMagnitude = areaRune.AreaMagnitude;
            }
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public AreaType AreaType { get; set; }

        public int AreaMagnitude { get; set; }

        public static bool operator ==(AreaRune left, AreaRune right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.Name == right.Name && left.Description == right.Description && left.AreaType == right.AreaType && left.AreaMagnitude == right.AreaMagnitude;
        }

        public static bool operator !=(AreaRune left, AreaRune right)
        {
            return !(left == right);
        }
    }

    public class EffectRune
    {
        public EffectRune()
        { }

        public EffectRune(EffectRune effectRune)
        {
            if (effectRune != null)
            {
                Name = effectRune.Name;
                Description = effectRune.Description;
                Effects = new List<SpellEffect>(effectRune.Effects);
            }
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<SpellEffect> Effects;

        public static bool operator ==(EffectRune left, EffectRune right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.Name == right.Name && left.Description == right.Description && left.Effects.All(effectLeft => right.Effects.Any(effectRight => effectLeft == effectRight));
        }

        public static bool operator !=(EffectRune left, EffectRune right)
        {
            return !(left == right);
        }
    }
}
