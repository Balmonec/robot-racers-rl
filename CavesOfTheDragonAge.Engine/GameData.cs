﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.Dungeons;

namespace CavesOfTheDragonAge.Engine
{
    /// <summary>
    /// A static class containing the game data.
    /// </summary>
    public static class GameData
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Loads the Tiles, Liquids, and Features, then sets the UI.
        /// </summary>
        /// <param name="userInterface">The IUserInterface to use as the UI.</param>
        /// <param name="tileFormats">The list of TileFormat objects to load in the TileFactory.</param>
        public static void Setup(IUserInterface userInterface, Dictionary<string, TileFormat> tileFormats = null, Dictionary<string, CreatureFormat> creatureFormats = null)
        {
            AutomationWaitTime = 500;
            TileFactory.LoadTileFormats(tileFormats);
            CreatureFactory.LoadCreatureFormats(creatureFormats);
            Ui = userInterface;
            loadTiles();
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Load the list of available Tiles.
        /// </summary>
        private static void loadTiles()
        {
            Tiles = new Dictionary<string, Tile>();

            Tiles.Add("STONE_WALL",
                new Tile("STONE_WALL",
                    new DisplayCharacter('#', Color.DarkGray),
                    Direction.Undefined,
                    true,
                    true,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME",
                                         "ON_TRIGGER:MOSS:ALTER_SELF:STONE_WALL_MOSSY",
                                         "ON_TRIGGER:DIG:ALTER_SELF:STONE_FLOOR"}));

            Tiles.Add("STONE_FLOOR",
                new Tile("STONE_FLOOR",
                    new DisplayCharacter('.', Color.LightGray),
                    Direction.Undefined,
                    false,
                    false,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORSILL",
                                         "CLASS:POSSIBLE_DOORWAY",
                                         "ON_TRIGGER:MOSS:ALTER_SELF:STONE_FLOOR_MOSSY" }));

            Tiles.Add("STONE_DOWNWARD_STAIR",
                new Tile("STONE_DOWNWARD_STAIR",
                    new DisplayCharacter('>', Color.White, Color.DarkGray),
                    Direction.Undefined,
                    false,
                    false,
                    null,
                    new List<string>() { "ON_CREATURE:MOVE_DOWNWARD" }));

            Tiles.Add("STONE_CAVITY",
                new Tile("STONE_CAVITY",
                    new DisplayCharacter('.', Color.Gray),
                    Direction.Undefined,
                    false,
                    false,
                    null,
                    new List<string>() { "HOLDS_LIQUID" }));

            Tiles.Add("STONE_WALL_MOSSY",
                new Tile("STONE_WALL_MOSSY",
                    new DisplayCharacter('#', Color.DarkGreen),
                    Direction.Undefined,
                    true,
                    true,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME",
                                         "TRIGGER_ADJACENT:MOSS" }));

            Tiles.Add("STONE_FLOOR_MOSSY",
                new Tile("STONE_FLOOR_MOSSY",
                    new DisplayCharacter('.', Color.DarkGreen),
                    Direction.Undefined,
                    false,
                    false,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORSILL",
                                         "CLASS:POSSIBLE_DOORWAY",
                                         "TRIGGER_ADJACENT:MOSS" }));

            Tiles.Add("SOLID_WALL",
                new Tile("SOLID_WALL",
                    new DisplayCharacter('\u2588', Color.DarkGray),
                    Direction.Undefined,
                    true,
                    true,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME" }));

            Tiles.Add("SMOOTH_WALL",
                new Tile("SMOOTH_WALL",
                    new DisplayCharacter('\u256C', Color.DarkGray),
                    Direction.Undefined,
                    true,
                    true,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME" }));

            Tiles.Add("WALL_OF_FORCE",
                new Tile("WALL_OF_FORCE",
                    new DisplayCharacter('#', Color.GreenYellow),
                    Direction.Undefined,
                    true,
                    true,
                    null,
                    new List<string>() { "CLASS:POSSIBLE_DOORFRAME"}));
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The number of milliseconds to wait in between actions when automatically moving the player.
        /// </summary>
        public static int AutomationWaitTime { get; set; }

        /// <summary>
        /// The identifier of the current dungeon being used.
        /// </summary>
        public static string CurrentDungeon { get; set; }

        /// <summary>
        /// The format of the current level, if you want to start over.
        /// </summary>
        public static DungeonFormat CurrentFormat { get; set; }

        /// <summary>
        /// The list of Dungeons in the game.
        /// </summary>
        public static Dictionary<string, Dungeon> DungeonMaps { get; set; }

        //TODO figure out a more elegant way to end the game
        //This means the public static bool, not the isWindowClosed hack.
        public static bool EndGame { get; set; }

        /// <summary>
        /// The list of available Tiles.
        /// </summary>
        public static Dictionary<string, Tile> Tiles { get; set; }

        /// <summary>
        /// The user interface currently configured to be used.
        /// </summary>
        public static IUserInterface Ui { get; set; }

        public static bool GoDown = false;

        public static bool StartOver = false;

        #endregion [-- Properties --]
    }
}