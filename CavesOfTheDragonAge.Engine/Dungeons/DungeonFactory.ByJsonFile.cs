﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class DungeonFactory
    {
        public static Dungeon ByJsonFile(DungeonFormat format)
        {
            Dungeon mapDungeon = new Dungeon();

            mapDungeon.Name = format.LevelName;

            int height = format.LevelMap.Count();
            int width = 0;
            for (int y = 0; y < height; y++)
            {
                if(format.LevelMap[y].Length > width)
                {
                    width = format.LevelMap[y].Length;
                }
            }

            mapDungeon.Tiles = makeTiles(format, width, height);
            mapDungeon.Creatures = makeCreatures(format, width, height, out Dictionary<string, Creature> creatureList);
            mapDungeon.CreatureList = creatureList;

            mapDungeon.ClearSFX();

            mapDungeon.MapToFlag = makeMapToFlag(mapDungeon.Tiles, width, height);

            return mapDungeon;
        }

        public static List<Tuple<string, DungeonFormat>> GetAvailableLevels()
        {
            var availableFormats = new List<Tuple<string, DungeonFormat>>();

            var datafiles = Directory.GetFiles(LEVEL_FOLDER, "*.json", SearchOption.TopDirectoryOnly)
                .Select(file => Path.GetFileName(file)).ToList();

            foreach (var file in datafiles)
            {
                var format = loadDungeonFormat(LEVEL_FOLDER + file);

                availableFormats.Add(new Tuple<string, DungeonFormat>(format.LevelName, format));
            }

            return availableFormats.OrderBy(f => f.Item2.DisplayOrder).ToList();
        }

        private static DungeonFormat loadDungeonFormat(string filename)
        {
            JsonSerializer serializer = new JsonSerializer();

            using (StreamReader sr = new StreamReader(filename))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<DungeonFormat>(reader);
            }
        }

        private static Tile[,] makeTiles(DungeonFormat format, int width, int height)
        {
            Tile[,] tiles = new Tile[width, height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (format.Tiles.TryGetValue(format.LevelMap[y][x].ToString(), out TileDirection tileType))
                    {
                        tiles[x, y] = TileFactory.GetTile(tileType.Tile, tileType.Direction);
                    }
                    else
                    {
                        tiles[x, y] = TileFactory.GetTile();
                    }
                }
            }

            return tiles;
        }

        private static string[,] makeCreatures(DungeonFormat format, int width, int height, out Dictionary<string, Creature> creatureList)
        {
            creatureList = new Dictionary<string, Creature>();
            string[,] creatures = new string[width, height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if(format.Creatures.TryGetValue(format.LevelMap[y][x].ToString(), out CreatureDirection creatureType))
                    {
                        Creature newCreature = CreatureFactory.GetNewCreature(creatureType.Creature, creatureList.Count, creatureType.Direction);
                        creatureList.Add(newCreature.Identifier, newCreature);
                        creatures[x, y] = newCreature.Identifier;
                    }
                    else
                    {
                        creatures[x, y] = "";
                    }
                }
            }

            return creatures;
        }

        private static DijkstraMap makeMapToFlag(Tile[,] tiles, int width, int height)
        {
            var dmap = new DijkstraMap(width, height);
            var goalCells = new bool[width, height];
            var impassibleCells = new bool[width, height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    goalCells[x, y] = tiles[x, y].Attributes.Contains("Goal");

                    impassibleCells[x, y] = tiles[x, y].BlocksMovement || tiles[x, y].Attributes.Contains("Deadly");
                }
            }

            dmap.BuildMap(goalCells, impassibleCells);

            return dmap;
        }

        private const string LEVEL_FOLDER = @".\data\levels\";
    }
}
