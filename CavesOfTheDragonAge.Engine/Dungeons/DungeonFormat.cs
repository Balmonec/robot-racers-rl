﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public class DungeonFormat
    {
        public string LevelName { get; set; }
        public int? DisplayOrder { get; set; }
        public List<string> StartingText { get; set; }
        public List<string> LevelMap { get; set; }
        public Dictionary<string, TileDirection> Tiles { get; set; }
        public Dictionary<string, CreatureDirection> Creatures { get; set; }

        public string GetStartingText()
        {
            string text = "";
            if (StartingText != null)
            {
                foreach (var line in StartingText)
                {
                    text += line + System.Environment.NewLine;
                }
            }

            return text;
        }
    }

    public class TileDirection
    {
        public string Tile { get; set; }
        public Direction Direction { get; set; }
    }

    public class CreatureDirection
    {
        public string Creature { get; set; }
        public Direction Direction { get; set; }
    }
}
