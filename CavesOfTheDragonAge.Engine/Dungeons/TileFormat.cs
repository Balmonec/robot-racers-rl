﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public class TileFormat
    {
        public DisplayCharacter Appearance { get; set; }
        public bool BlocksVision { get; set; }
        public bool BlocksMovement { get; set; }
        public Dictionary<Direction, DisplayCharacter> DisplayDirections { get; set; }
        public List<string> Attributes { get; set; }
    }
}
