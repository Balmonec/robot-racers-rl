﻿using System;
using System.Collections.Generic;
using System.Linq;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Get the DisplayCharacter for the given square.
        /// </summary>
        /// <param name="x">The X coordinate of the square.</param>
        /// <param name="y">The Y coordinate of the square.</param>
        /// <returns>The DisplayCharacter for the given square.</returns>
        public DisplayCharacter GetDisplayValue(int x, int y)
        {
            Tile currentTile;
            Creature currentCreature;

            if (x < 0 || x >= Width || y < 0 || y >= Height)
            {
                return new DisplayCharacter();
            }

            if (SFXLayer[x, y] != null)
            {
                return SFXLayer[x, y];
            }
            else if (CreatureList.TryGetValue(Creatures[x, y], out currentCreature) && currentCreature.GetDirectionalAppearance() != null)
            {
                return currentCreature.GetDirectionalAppearance();
            }
            else if (Tiles[x, y]?.GetDirectionalAppearance() != null)
            {
                return Tiles[x, y]?.GetDirectionalAppearance();
            }
            else
            {
                return new DisplayCharacter();
            }

        }

        /// <summary>
        /// Get the DisplayCharacter for the given square.
        /// </summary>
        /// <param name="location">The Location of the square.</param>
        /// <returns>The DisplayCharacter for the given square.</returns>
        public DisplayCharacter GetDisplayValue(Location location)
        {
            return GetDisplayValue(location.X, location.Y);
        }

        /// <summary>
        /// Determine the list of directions that respond to the given trigger from a given tile.
        /// </summary>
        /// <param name="location">The location of the tile to start from.</param>
        /// <param name="trigger">The trigger to check.</param>
        /// <returns>A list of directions that that respond to the given trigger from the given tile.</returns>
        public List<Direction> GetTriggerDirections(Location location, string trigger)
        {
            return Enum.GetValues(typeof (Direction)).Cast<Direction>()
                .Where(direction => HasDirectionTrigger(location, direction, trigger)).ToList();
        }

        /// <summary>
        /// Determine if any items one square in the given direction will respond to the given trigger.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="direction">The Direction to check.</param>
        /// <param name="trigger">The trigger to check.</param>
        /// <returns>Returns TRUE if an item in the given location will respond to the given trigger, and FALSE otherwise.</returns>
        public bool HasDirectionTrigger(Location location, Direction direction, string trigger)
        {
            if (direction.IsHorizontalMovement() || direction == Direction.Here)
            {
                return HasTrigger(location.GetVector(direction), trigger);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determines if any items in the given location will respond to the given trigger.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="trigger">The trigger to check.</param>
        /// <returns>Returns TRUE if an item in the given location will respond to the given trigger, and FALSE otherwise.</returns>
        public bool HasTrigger(Location location, string trigger)
        {
            if ((location.X < 0) || (location.Y < 0) || (location.X >= Width) || (location.Y >= Height))
            {
                return false;
            }
            else
            {
                return (hasTileTrigger(location, trigger));
            }
        }

        /// <summary>
        /// Trigger an action in all cells adjacent to the selected cell.
        /// </summary>
        /// <param name="x">The X coordinate of the cell.</param>
        /// <param name="y">The Y coordinate of the cell.</param>
        /// <param name="trigger">The name of the trigger.</param>
        public void TriggerAdjacent(int x, int y, string trigger)
        {
            //Update the buffers to clean copies.
            _tilesBuffer = copyTiles();

            internalTriggerAdjacent(x, y, trigger);

            //Flush the buffers to the main map.
            Tiles = _tilesBuffer;
        }

        /// <summary>
        /// Trigger an action in all cells adjacent to the selected cell.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="trigger">The name of the trigger.</param>
        public void TriggerAdjacent(Location location, string trigger)
        {
            TriggerAdjacent(location.X, location.Y, trigger);
        }

        /// <summary>
        /// Trigger an action in the selected cell.
        /// </summary>
        /// <param name="x">The X coordinate of the cell.</param>
        /// <param name="y">The Y coordinate of the cell.</param>
        /// <param name="trigger">The name of the trigger.</param>
        public void TriggerCell(int x, int y, string trigger)
        {
            //Update the buffers to clean copies.
            _tilesBuffer = copyTiles();

            internalTriggerCell(x, y, trigger);

            //Flush the buffers to the main map.
            Tiles = _tilesBuffer;
        }

        /// <summary>
        /// Trigger an action in the selected cell.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="trigger">The name of the trigger.</param>
        public void TriggerCell(Location location, string trigger)
        {
            TriggerCell(location.X, location.Y, trigger);
        }

        /// <summary>
        /// Cycle through each of the layers and update based on cell behavior.
        /// </summary>
        public void UpdateCells()
        {
            //Update the buffers to clean copies.
            _tilesBuffer = copyTiles();

            updateTiles();

            //Flush the buffers to the main map.
            Tiles = _tilesBuffer;
            ClearSFX();
        }

        /// <summary>
        /// Returns true if the given coordinate pair is inside the bounds of the dungeon.
        /// </summary>
        /// <param name="x">The x-coordinate to check.</param>
        /// <param name="y">The y-coordinate to check.</param>
        /// <returns>Returns true if the given coordinate pair is inside the bounds of the dungeon.</returns>
        public bool IsInDungeon(int x, int y)
        {
            return Tiles.IndexInBounds2D(x, y);
        }

        /// <summary>
        /// Returns true if the given location is inside the bounds of the dungeon.
        /// </summary>
        /// <param name="x">The x-coordinate to check.</param>
        /// <param name="y">The y-coordinate to check.</param>
        /// <returns>Returns true if the given coordinate pair is inside the bounds of the dungeon.</returns>
        public bool IsInDungeon(Location location)
        {
            return IsInDungeon(location.X, location.Y);
        }

        public void ClearSFX()
        {
            SFXLayer = new DisplayCharacter[Width, Height];
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Trigger an action in all cells adjacent to the selected cell.
        /// </summary>
        /// <param name="x">The X coordinate of the cell.</param>
        /// <param name="y">The Y coordinate of the cell.</param>
        /// <param name="trigger">The name of the trigger.</param>
        private void internalTriggerAdjacent(int x, int y, string trigger)
        {
            for (int a = x - 1; a <= x + 1; a++)
            {
                if (a >= 0 && a < Width)
                {
                    for (int b = y - 1; b <= y + 1; b++)
                    {
                        if (b >= 0 && b < Height
                            && (a != x || b != y))
                        {
                            internalTriggerCell(a, b, trigger);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Trigger an action in the selected cell, without updating the buffer.
        /// </summary>
        /// <param name="x">The X coordinate of the cell.</param>
        /// <param name="y">The Y coordinate of the cell.</param>
        /// <param name="trigger">The name of the trigger.</param>
        private void internalTriggerCell(int x, int y, string trigger)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                triggerTile(x, y, trigger);
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        public int Height
        {
            get { return Tiles.GetLength(1); }
        }

        public string Name { get; set; }

        public int Width
        {
            get { return Tiles.GetLength(0); }
        }

        public DisplayCharacter[,] SFXLayer;

        public DijkstraMap MapToFlag;

        #endregion [-- Properties --]
    }
}