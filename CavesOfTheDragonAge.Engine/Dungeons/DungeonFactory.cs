﻿using CavesOfTheDragonAge.Engine.Creatures;
using System;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    /// <summary>
    /// A factory class for the Dungeon object, containing various dungeon generation algorithms.
    /// </summary>
    public static partial class DungeonFactory
    {
        /// <summary>
        /// Generates a new Dungeon, and returns it.
        /// </summary>
        /// <param name="generator">The type of the dungeon generator to use when generating the dungeon.</param>
        /// <param name="name">The name of the new dungeon.</param>
        /// <returns>A new Dungeon, as created by the selected generator.</returns>
        public static Dungeon GetDungeon(GeneratorType generator, string name, Tuple<int, int> size, Creature player = null)
        {
            switch (generator)
            {
                case GeneratorType.ManualTesting:
                default:
                    return manualTestGenerator();
            }
        }

        /// <summary>
        /// Generates a new Dungeon following the pattern of the map string, and returns it.
        /// </summary>
        /// <param name="map">A multi-line string containing the map to generate.</param>
        /// <returns>A new Dungeon, as defined by the simple map.</returns>
        public static Dungeon GetDungeonBySimpleMap(string map)
        {
            return bySimpleMapGenerator(map);
        }

        public enum GeneratorType
        {
            ManualTesting
        }
    }
}