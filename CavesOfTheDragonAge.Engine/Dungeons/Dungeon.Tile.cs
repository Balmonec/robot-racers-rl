﻿using System;
using System.Linq;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    //TODO: Modify this to have 1 method of each type with a Layer parameter, rather than having 3-4 different identical methods that each affect a different layer.
    public partial class Dungeon
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Get the Tile object for a given square.
        /// </summary>
        /// <param name="x">The X coordinate for the Tile to return.</param>
        /// <param name="y">The Y coordinate for the Tile to return.</param>
        /// <returns>The Tile object in the given square.</returns>
        public Tile GetTile(int x, int y)
        {
            if (!Tiles.IndexInBounds2D(x, y))
            {
                return new Tile();
            }
            else
            {
                return Tiles[x, y];
            }
        }

        /// <summary>
        /// Get the Tile object for a given square.
        /// </summary>
        /// <param name="location">The Location for the Tile to return.</param>
        /// <returns>The Tile object in the given square.</returns>
        public Tile GetTile(Location location)
        {
            return GetTile(location.X, location.Y);
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Alter the adjacent Tile tiles.
        /// </summary>
        /// <param name="x">The X coordinate of the originating tile.</param>
        /// <param name="y">The Y coordinate of the originating tile.</param>
        /// <param name="attribute">The complete attribute containing the information to change the dungeon tile from and to.</param>
        private void alterAdjacentTile(int x, int y, string attribute)
        {
            for (int a = x - 1; a <= x + 1; a++)
            {
                if (a >= 0 && a < Width)
                {
                    for (int b = y - 1; b <= y + 1; b++)
                    {
                        if (b >= 0 && b < Height
                            && (a != x || b != y)
                            && attribute.Split(':')[1] == Tiles[a, b].Type)
                        {
                            _tilesBuffer[a, b] = TileFactory.GetTile(attribute.Split(':')[2]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Alter the Tile type immediately.
        /// </summary>
        /// <param name="x">The X coordinate of the Tile.</param>
        /// <param name="y">The Y coordinate of the Tile.</param>
        /// <param name="attribute">The complete attribute containing the information to change the dungeon tile to.</param>
        private void alterSelfImmediateTile(int x, int y, string attribute)
        {
            alterSelfTile(x, y, attribute);
            Tiles[x, y] = TileFactory.GetTile(attribute.Split(':')[1]);
        }

        /// <summary>
        /// Alter the Tile type.
        /// </summary>
        /// <param name="x">The X coordinate of the Tile.</param>
        /// <param name="y">The Y coordinate of the Tile.</param>
        /// <param name="attribute">The complete attribute containing the information to change the dungeon tile to.</param>
        private void alterSelfTile(int x, int y, string attribute)
        {
            _tilesBuffer[x, y] = TileFactory.GetTile(attribute.Split(':')[1]);
        }

        /// <summary>
        /// Return a copy of the Tile layer to use when updating the cells.
        /// </summary>
        /// <returns>A copy of the Tile layer</returns>
        private Tile[,] copyTiles()
        {
            Tile[,] newTiles = new Tile[Width, Height];

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    newTiles[x, y] = new Tile(Tiles[x, y]);
                }
            }

            return newTiles;
        }

        /// <summary>
        /// Determines if the Tile in the given location will respond to the given trigger.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="trigger">The trigger to check.</param>
        /// <returns>Returns TRUE if the Tile in the given location will respond to the given trigger, and FALSE otherwise.</returns>
        private bool hasTileTrigger(Location location, string trigger)
        {
            return GetTile(location).Attributes
                .Any(attribute => attribute.StartsWith("ON_TRIGGER:" + trigger));
        }

        /// <summary>
        /// Process the attribute of a Tile to see if it requires action.
        /// </summary>
        /// <param name="x">The X coordinate of the Tile.</param>
        /// <param name="y">The Y coordinate of the Tile.</param>
        /// <param name="attribute">The complete attribute.</param>
        private void processTileAttribute(int x, int y, string attribute)
        {
            if (attribute.Split(':')[0] == "ALTER_ADJACENT_TILE")
            {
                alterAdjacentTile(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "ALTER_SELF")
            {
                alterSelfTile(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "ALTER_SELF_IMMEDIATE")
            {
                alterSelfImmediateTile(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "TRIGGER_ADJACENT")
            {
                internalTriggerAdjacent(x, y, attribute.Split(':')[1]);
            }
            else if (attribute.Split(':')[0] == "TRIGGER_SELF")
            {
                internalTriggerCell(x, y, attribute.Split(':')[1]);
            }
            else if (attribute.Split(':')[0] == "ON_CREATURE" && Creatures[x, y] != "")
            {
                processTileAttribute(x, y, attribute.Split(new char[] { ':' }, 2)[1]);
            }
            else if (attribute.Split(':')[0] == "MOVE_DOWNWARD")
            {
                if (Creatures[x, y] == "PLAYER")
                {
                    GameData.GoDown = true;
                }
            }
        }

        /// <summary>
        /// Trigger an action in the selected tile.
        /// </summary>
        /// <param name="x">The X coordinate of the tile.</param>
        /// <param name="y">The Y coordinate of the tile.</param>
        /// <param name="trigger">The name of the trigger.</param>
        private void triggerTile(int x, int y, string trigger)
        {
            foreach (string attribute in GetTile(x, y).Attributes)
            {
                if (attribute.StartsWith("ON_TRIGGER:" + trigger))
                {
                    processTileAttribute(x, y, attribute.Split(new char[] { ':' }, 3)[2]);
                }
            }
        }

        /// <summary>
        /// Cycle through the Tile layer and update based on cell behavior.
        /// </summary>
        private void updateTiles()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    foreach (string attribute in GetTile(x, y).Attributes)
                    {
                        processTileAttribute(x, y, attribute);
                    }
                }
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// An array of strings indicating the map of Tile objects.
        /// </summary>
        public Tile[,] Tiles
        {
            get { return _tiles; }
            set { _tiles = value; }
        }

        /// <summary>
        /// The internal string array holding the map of Tile objects.
        /// </summary>
        private Tile[,] _tiles;

        /// <summary>
        /// The internal buffer of the tiles.
        /// </summary>
        private Tile[,] _tilesBuffer;

        #endregion [-- Properties --]
    }
}