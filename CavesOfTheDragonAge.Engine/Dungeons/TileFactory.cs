﻿using CavesOfTheDragonAge.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public static class TileFactory
    {
        /// <summary>
        /// Load the factory with the available tile formats.
        /// </summary>
        /// <param name="tileFormats">A list of tile formats to use.  If not sent, the tile formats
        /// will be loaded from data files.</param>
        public static void LoadTileFormats(Dictionary<string, TileFormat> tileFormats = null)
        {
            JsonSerializer serializer = new JsonSerializer();
            TileFormats = new Dictionary<string, TileFormat>();

            if(tileFormats != null)
            {
                TileFormats = tileFormats;
            }
            else if(Directory.Exists(TILE_FOLDER))
            {
                var datafiles = Directory.GetFiles(TILE_FOLDER, "*.json", SearchOption.TopDirectoryOnly)
                    .Select(file => Path.GetFileName(file)).ToList();

                foreach(var file in datafiles)
                {
                    using (StreamReader sr = new StreamReader(TILE_FOLDER + file))
                    using (JsonReader reader = new JsonTextReader(sr))
                    {
                        serializer.Deserialize<Dictionary<string, TileFormat>>(reader)
                            .ToList().ForEach(item => TileFormats[item.Key] = item.Value);
                    }
                }
            }
        }

        public static Tile GetTile(string tileType = "", Direction direction = Direction.Undefined)
        {
            if (TileFormats.ContainsKey(tileType))
            {
                var format = TileFormats[tileType];
                return new Tile(
                    tileType,
                    new DisplayCharacter(format.Appearance),
                    direction,
                    format.BlocksVision,
                    format.BlocksMovement,
                    format.DisplayDirections != null ? new Dictionary<Direction, DisplayCharacter>(format.DisplayDirections) : null,
                    format.Attributes != null ? new List<string>(format.Attributes) : null
                    );
            }
            else
            {
                return new Tile();
            }
        }

        private static Dictionary<string, TileFormat> TileFormats = new Dictionary<string, TileFormat>();

        private const string TILE_FOLDER = @".\data\tiles\";
    }
}
