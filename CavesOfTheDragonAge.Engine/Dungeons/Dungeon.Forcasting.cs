﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        public List<Tuple<Location, Direction, bool>> ForcastMovements(Location startingLocation, Direction startingDirection, Instruction[] queuedInstructions)
        {
            var anticipatedLocations = new List<Tuple<Location, Direction, bool>>();

            anticipatedLocations.Add(new Tuple<Location, Direction, bool>(startingLocation, startingDirection, true));

            foreach(var inst in queuedInstructions ?? new Instruction[0])
            {
                if (inst == null)
                    break;

                foreach(var vector in inst.GetVectors())
                {
                    var oldLocation = anticipatedLocations.Last().Item1;
                    var oldDirection = anticipatedLocations.Last().Item2;
                    var newLocation = oldLocation.GetVector(vector.Item1.AlignTo(oldDirection));

                    anticipatedLocations.Add(new Tuple<Location, Direction, bool>(IsPassable(newLocation) ? newLocation : oldLocation, vector.Item2.AlignTo(oldDirection), false));
                    if(GetTile(newLocation).Attributes.Contains("Deadly"))
                    {
                        return anticipatedLocations;
                    }
                }

                //check for fast conveyors
                {
                    var oldLocation = anticipatedLocations.Last().Item1;
                    var tile = GetTile(oldLocation);
                    if (tile.Attributes.Contains("FastPush")
                        && IsDirectionPassable(anticipatedLocations.Last().Item1, tile.Direction))
                    {
                        anticipatedLocations.Add(new Tuple<Location, Direction, bool>(oldLocation.GetVector(tile.Direction), anticipatedLocations.Last().Item2, false));
                    }
                }

                //check for conveyors
                {
                    var oldLocation = anticipatedLocations.Last().Item1;
                    var tile = GetTile(oldLocation);
                    if (tile.Attributes.Contains("Push")
                        && IsDirectionPassable(anticipatedLocations.Last().Item1, tile.Direction))
                    {
                        anticipatedLocations.Add(new Tuple<Location, Direction, bool>(oldLocation.GetVector(tile.Direction), anticipatedLocations.Last().Item2, false));
                    }
                }

                //check for rotaters
                {
                    var oldLocation = anticipatedLocations.Last().Item1;
                    var tile = GetTile(oldLocation);
                    if (tile.Attributes.Contains("Rotate"))
                    {
                        anticipatedLocations.Add(new Tuple<Location, Direction, bool>(oldLocation, tile.Direction.AlignTo(anticipatedLocations.Last().Item2), false));
                    }
                }

                anticipatedLocations[anticipatedLocations.Count - 1] = new Tuple<Location, Direction, bool>(
                    anticipatedLocations[anticipatedLocations.Count - 1].Item1,
                    anticipatedLocations[anticipatedLocations.Count - 1].Item2,
                    true);
            }

            return anticipatedLocations;
        }

        public int ForecastFinalFlagDistance(Location startingLocation, Direction startingDirection, Instruction[] testInstructions)
        {
            var locations = GameData.DungeonMaps[GameData.CurrentDungeon].ForcastMovements(startingLocation, startingDirection, testInstructions);
            var lastPostion = locations.Last().Item1;
            var distance = GameData.DungeonMaps[GameData.CurrentDungeon].MapToFlag.GoalMap(lastPostion.X, lastPostion.Y);
            if (locations.Any(loc => loc.Item3 == true && GameData.DungeonMaps[GameData.CurrentDungeon].MapToFlag.GoalMap(loc.Item1.X, loc.Item1.Y) == 0))
            {
                distance = 0;
            }

            return distance;
        }

        public Instruction[] DetermineBestMove(Location startingLocation, Direction startingDirection, List<Tuple<Instruction, int>> availableInstructions)
        {
            var instructionList = new List<string>();

            foreach (var inst in availableInstructions)
            {
                for (int c = 0; c < inst.Item2; c++)
                {
                    instructionList.Add(inst.Item1.Identifier);
                }
            }

            var testInstructions = new List<Tuple<Instruction[], int>>();

            int numCards = instructionList.Count;

            for (int a = 0; a < numCards; a++)
            {
                for (int b = 0; b < numCards; b++)
                {
                    if (a == b)
                        continue;

                    for (int c = 0; c < numCards; c++)
                    {
                        if (a == c || b == c)
                            continue;

                        for (int d = 0; d < numCards; d++)
                        {
                            if (a == d || b == d || c == d)
                                continue;

                            for (int e = 0; e < numCards; e++)
                            {
                                if (a == e || b == e || c == e || d == e)
                                    continue;


                                var instr = new Instruction[5];
                                instr[0] = InstructionFactory.GetInstruction(instructionList[a]);
                                instr[1] = InstructionFactory.GetInstruction(instructionList[b]);
                                instr[2] = InstructionFactory.GetInstruction(instructionList[c]);
                                instr[3] = InstructionFactory.GetInstruction(instructionList[d]);
                                instr[4] = InstructionFactory.GetInstruction(instructionList[e]);

                                var distance = GameData.DungeonMaps[GameData.CurrentDungeon].ForecastFinalFlagDistance(startingLocation,
                                    startingDirection, instr);

                                testInstructions.Add(new Tuple<Instruction[], int>(instr, distance));
                            }
                        }
                    }
                }
            }

            var orderedList = testInstructions.OrderBy(i => i.Item2);

            var debugList = new List<string>();
            foreach (var entry in orderedList)
            {
                debugList.Add(entry.Item1[0].Identifier + ", " +
                    entry.Item1[1].Identifier + ", " +
                    entry.Item1[2].Identifier + ", " +
                    entry.Item1[3].Identifier + ", " +
                    entry.Item1[4].Identifier + ", " +
                    entry.Item2);
            }


            return orderedList.First().Item1;
        }
    }
}
