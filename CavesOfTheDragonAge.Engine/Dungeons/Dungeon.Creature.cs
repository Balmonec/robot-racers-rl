﻿using System;
using System.Collections.Generic;
using System.Linq;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using System.Drawing;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Get the Creature object for a given square.
        /// </summary>
        /// <param name="x">The X coordinate for the Creature to return.</param>
        /// <param name="y">The Y coordinate for the Creature to return.</param>
        /// <returns>The Creature object in the given square.</returns>
        public Creature GetCreature(int x, int y)
        {
            Creature currentCreature;

            if(!Creatures.IndexInBounds2D(x, y) || !CreatureList.TryGetValue(Creatures[x, y], out currentCreature))
            {
                currentCreature = null;
            }

            return currentCreature;
        }

        /// <summary>
        /// Get the Creature object for a given square.
        /// </summary>
        /// <param name="location">The Location for the Creature to return.</param>
        /// <returns>The Creature object in the given square.</returns>
        public Creature GetCreature(Location location)
        {
            return GetCreature(location.X, location.Y);
        }

        /// <summary>
        /// Get the Creature object for the given Id.
        /// </summary>
        /// <param name="creatureId">The Identifier of the Creature to return.</param>
        /// <returns>The Creature object with the given Id.</returns>
        public Creature GetCreature(string creatureId)
        {
            if (CreatureList.ContainsKey(creatureId))
            {
                return CreatureList[creatureId];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get the location of the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to locate.</param>
        /// <returns>A Location indicating the location of the specified creature.</returns>
        public Location GetCreatureLocation(string creatureId)
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (Creatures[x, y] == creatureId)
                    {
                        return new Location(x, y);
                    }
                }
            }

            return new Location(-1, -1);
        }

        /// <summary>
        /// Get the X coordinate of the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to locate.</param>
        /// <returns>The X coordinate of the specified creature.</returns>
        public int GetCreatureX(string creatureId)
        {
            return GetCreatureLocation(creatureId).Item1;
        }

        /// <summary>
        /// Get the Y coordinate of the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to locate.</param>
        /// <returns>The Y coordinate of the specified creature.</returns>
        public int GetCreatureY(string creatureId)
        {
            return GetCreatureLocation(creatureId).Item2;
        }

        /// <summary>
        /// Determine the list of passable directions from a given tile.
        /// </summary>
        /// <param name="location">The location of the tile to start from.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>A list of directions that are passable from the given tile.</returns>
        public List<Direction> GetPassableDirections(Location location, string creature = "")
        {
            return Enum.GetValues(typeof (Direction)).Cast<Direction>().
                Where(direction => IsDirectionPassable(location, direction, creature)).ToList();
        }

        /// <summary>
        /// Determine if a tile one square in a specific direction is passable.
        /// </summary>
        /// <param name="location">The location of the tile to start from.</param>
        /// <param name="direction">The direction to check.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>Returns true if the tile is passable by the given creature, and false if it is not.</returns>
        public bool IsDirectionPassable(Location location, Direction direction, string creature = "")
        {
            if (direction.IsHorizontalMovement())
            {
                return IsPassable(location.GetVector(direction), creature);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determine if a specific tile is passable.
        /// </summary>
        /// <param name="x">The X coordinate of the tile to check.</param>
        /// <param name="y">The Y coordinate of the tile to check.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>Returns true if the tile is passable by the given creature, and false if it is not.</returns>
        public bool IsPassable(int x, int y, string creature = "")
        {
            if ((x < 0) || (y < 0) || (x >= Width) || (y >= Height))
            {
                return false;
            }
            else if (GetTile(x, y).BlocksMovement)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Determine if a specific tile is passable.
        /// </summary>
        /// <param name="location">The location of the tile to check.</param>
        /// <param name="creature">The identifier of the creature to check for.
        /// <para>If none is given, will assume a creature of default properties.</para></param>
        /// <returns>Returns true if the tile is passable by the given creature, and false if it is not.</returns>
        public bool IsPassable(Location location, string creature = "")
        {
            return IsPassable(location.X, location.Y, creature);
        }

        /// <summary>
        /// Move the creature in a specified direction.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to move.</param>
        /// <param name="direction">The direction to move the creature.</param>
        /// <returns>Returns true if the movement is successful, and false otherwise.</returns>
        public bool MoveCreature(string creatureId, Direction direction)
        {
            if(direction.GetDeltaX() == 0 && direction.GetDeltaY() == 0)
            {
                //moving to same square
                return true;
            }

            int oldX = GetCreatureX(creatureId);
            int oldY = GetCreatureY(creatureId);

            int newX = oldX + direction.GetDeltaX();
            int newY = oldY + direction.GetDeltaY();

            if (IsPassable(newX, newY) == false)
            {
                GameData.Ui.DisplayMessage(CreatureList[creatureId].Name + " is blocked!", Color.Yellow);
                return false;
            }
            else if (Creatures[newX, newY] != "")
            {
                string thisCreatureName = CreatureList[creatureId].Name;
                string thatCreatureName = CreatureList[Creatures[newX, newY]].Name;
                GameData.Ui.DisplayMessage(thisCreatureName + " bumps into " + thatCreatureName + "!", Color.Red);
                if (!MoveCreature(CreatureList[Creatures[newX, newY]].Identifier, direction))
                {
                    return false;
                }
            }

            Creatures[newX, newY] = creatureId;
            Creatures[oldX, oldY] = "";

            if(Tiles[newX, newY].Attributes.Contains("Deadly"))
            {
                KillCreature(creatureId);
            }

            return true;
        }

        /// <summary>
        /// Make the creature attack in a specified direction.
        /// </summary>
        /// <param name="attackerId">The ID of the creature performing the attack.</param>
        /// <param name="direction">The direction to attack in.</param>
        /// <returns></returns>
        public void Attack(string attackerId, Direction direction)
        {
            Location attackerLocation = GetCreatureLocation(attackerId);
            Location targetLocation = attackerLocation.GetVector(direction);
            Creature attackerCreature = CreatureList[attackerId];
            Creature targetCreature = GetCreature(targetLocation);

            if (targetCreature == null)
            {
                GameData.Ui.DisplayMessage(attackerCreature.Name + " swings wildly at the air to the " + direction.GetDescription() + "!", Color.White);
            }
            else
            {
                int damage = Randomization.RandomInt(1, 6);
                GameData.Ui.DisplayMessage(attackerCreature.Name + " hits " + targetCreature.Name + " for " + damage + " hit points of damage!", Color.Red);
                targetCreature.Health = targetCreature.Health - damage;
                attackerCreature.Score -= damage;
                targetCreature.Score -= damage;
            }
        }

        /// <summary>
        /// Kill the specified creature.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to kill.</param>
        public void KillCreature(string creatureId)
        {
            Creature creature = GetCreature(creatureId);
            Location creatureLocation = GetCreatureLocation(creatureId);
            Creatures[creatureLocation.X, creatureLocation.Y] = "";
            creature.Health.Current = 0;
            GameData.Ui.DisplayMessage(creature.Name + " has died!", GameColors.Red);
            if (creatureId == GameData.Ui.PlayerId)
            {
                GameData.Ui.RenderAll();
                int choice = GameData.Ui.DisplayMenu("Your robot has been destroyed." + Environment.NewLine
                                                   + "" + Environment.NewLine
                                                   + "You lose." + Environment.NewLine
                                                   + "" + Environment.NewLine
                                                   + "Would you like to try again?",
                                                   new List<string>() { "Yes", "No" }, false);
                GameData.EndGame = true;
                GameData.StartOver = choice == 0;
            }
        }

        /// <summary>
        /// Teleport the specified creature to the specified location.
        /// </summary>
        /// <param name="creatureId">The ID of the creature to teleport.</param>
        /// <param name="destination">The location to teleport to.</param>
        public void TeleportCreature(string creatureId, Location destination)
        {
            if (IsPassable(destination) != false && Creatures[destination.X, destination.Y] == "")
            {
                if (GetCreatureX(creatureId) != -1)
                {
                    Creatures[GetCreatureX(creatureId), GetCreatureY(creatureId)] = "";
                }
                Creatures[destination.X, destination.Y] = creatureId;
            }
        }

        public void ExecuteInstruction(string identifier, int register)
        {
            var creature = GetCreature(identifier);

            var instruction = creature?.QueuedInstructions[register];

            if(instruction != null)
            {
                var vectors = instruction.GetVectors();

                foreach(var vector in vectors)
                {
                    if(vector.Item1.IsHorizontalMovement())
                    {
                        MoveCreature(identifier, vector.Item1.AlignTo(creature.Facing));
                    }
                    creature.Facing = vector.Item2.AlignTo(creature.Facing);

                    if(GameData.EndGame == true)
                    {
                        break;
                    }

                    GameData.Ui.RenderAll(true, true);
                    System.Threading.Thread.Sleep(GameData.AutomationWaitTime);
                }
            }
        }

        /// <summary>
        /// Run the conveyors in the map
        /// </summary>
        /// <param name="conveyorAttribute">The attribute determining if the tile is a conveyor.</param>
        public void RunConveyors(string conveyorAttribute)
        {
            var creaturesToCheck = CreatureList.Keys.ToList();

            bool creaturesDisqualified = true;

            while(creaturesDisqualified)
            {
                creaturesDisqualified = false;
                foreach (var creatureId in CreatureList.Keys.Where(c => creaturesToCheck.Contains(c)))
                {
                    var oldLocation = GetCreatureLocation(creatureId);
                    var tile = GetTile(oldLocation);
                    var newLocation = oldLocation.GetVector(tile.Direction);
                    if (!tile.Attributes.Contains(conveyorAttribute))
                    {
                        //They aren't on a conveyor, so they shouldn't move.
                        creaturesToCheck.Remove(creatureId);
                        creaturesDisqualified = true;
                    }
                    else if (!IsPassable(newLocation) ||
                        (Creatures[newLocation.X, newLocation.Y] != "" && !creaturesToCheck.Contains(Creatures[newLocation.X, newLocation.Y])))
                    {
                        //They can't be pushed to the new location, so they shouldn't move.
                        creaturesToCheck.Remove(creatureId);
                        creaturesDisqualified = true;
                    }
                }
            }

            //check for multiple creatures pushed into the same tile
            List<Tuple<string, Location>> movingTo = new List<Tuple<string, Location>>();
            foreach (var creatureId in creaturesToCheck)
            {
                var oldLocation = GetCreatureLocation(creatureId);
                var tile = GetTile(oldLocation);
                var newLocation = oldLocation.GetVector(tile.Direction);

                movingTo.Add(new Tuple<string, Location>(creatureId, newLocation));
            }

            foreach(var move in movingTo)
            {
                if(movingTo.Count(x => x.Item2 == move.Item2) > 1)
                {
                    creaturesToCheck.Remove(move.Item1);
                }
            }

            creaturesToCheck.Sort((left, right) =>
            {
                if (movingTo.Any(m => m.Item1 == left && m.Item2 == GetCreatureLocation(right)))
                {
                    //left is moving onto right, so right must be done first.
                    return -1;
                }
                if (movingTo.Any(m => m.Item1 == right && m.Item2 == GetCreatureLocation(left)))
                {
                    //right is moving onto left, so left must be done first.
                    return 1;
                }
                //unrelated movement, can be done in either order.
                return 0;
            });

            foreach(var creatureId in creaturesToCheck)
            {
                var oldLocation = GetCreatureLocation(creatureId);
                var tile = GetTile(oldLocation);
                MoveCreature(creatureId, tile.Direction);
                GameData.Ui.RenderAll(true, true);
                System.Threading.Thread.Sleep(GameData.AutomationWaitTime);
            }
        }

        public void Rotate()
        {
            var creatures = CreatureList.Keys.ToList();

            foreach(var creatureId in creatures)
            {
                var location = GetCreatureLocation(creatureId);
                var tile = GetTile(location);
                if(tile.Attributes.Contains("Rotate"))
                {
                    CreatureList[creatureId].Facing = tile.Direction.AlignTo(CreatureList[creatureId].Facing);
                    GameData.Ui.RenderAll(true, true);
                    System.Threading.Thread.Sleep(GameData.AutomationWaitTime);
                }
            }
        }

        public void CheckWinCondition()
        {
            foreach(var creatureId in CreatureList.Keys)
            {
                var location = GetCreatureLocation(creatureId);
                var tile = GetTile(location);
                if(tile.Attributes.Contains("Goal"))
                {
                    var winner = CreatureList[creatureId];

                    if (winner.Mobile == true)
                    {
                        List<string> options = null;
                        string message = winner.Name + " has reached the goal!" + System.Environment.NewLine
                            + System.Environment.NewLine;

                        if (creatureId == GameData.Ui.PlayerId)
                        {
                            message += "You have Won!";
                        }
                        else
                        {
                            message += "You have lost." + Environment.NewLine
                                     + "" + Environment.NewLine
                                     + "Would you like to try again?";
                            options = new List<string>() { "Yes", "No" };
                        }

                        int choice = GameData.Ui.DisplayMenu(message, options, false);

                        GameData.EndGame = true;
                        GameData.StartOver = choice == 0;
                        break;
                    }
                }
            }
        }

        ///// <summary>
        ///// Determines the PathwayType of the square.
        ///// </summary>
        ///// <param name="location">The location of the square to check.</param>
        ///// <param name="creature">The identifier of the creature to check for.
        ///// <para>If none is given, will assume a creature of default properties.</para></param>
        ///// <returns>The PathwayType of the square, as determined by its surroundings.</returns>
        //public PathwayType GetPathwayType(Location location, string creature = "")
        //{
        //    List<Direction> passableDirections = GetPassableDirections(location, creature);

        //    if (IsPassable(location, creature) == false)
        //    {
        //        return PathwayType.Impassable;
        //    }
        //    else if (passableDirections.Count == 1)
        //    {
        //        return PathwayType.DeadEnd;
        //    }
        //    else if (passableDirections.Count >= 3)
        //    {
        //        return PathwayType.Room;
        //    }
        //    else if (passableDirections.Count == 2)
        //    {
        //        if (passableDirections.Contains(passableDirections[0].Opposite()))
        //        {
        //            return PathwayType.CorridorStraight;
        //        }
        //        else
        //        {
        //            return PathwayType.CorridorCorner;
        //        }
        //    }
        //    else
        //    {
        //        return PathwayType.Undefined;
        //    }
        //}

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        /// <summary>
        /// A Dictionary holding the Creature objects.
        /// </summary>
        public Dictionary<string, Creature> CreatureList { get; set; }

        /// <summary>
        /// An array of strings indicating the map of Creatures.
        /// </summary>
        public string[,] Creatures { get; set; }

        #endregion [-- Properties --]
    }
}