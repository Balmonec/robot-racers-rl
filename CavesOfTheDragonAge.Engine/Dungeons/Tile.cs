﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    /// <summary>
    /// A single square of the dungeon terrain.
    /// </summary>
    public class Tile
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Construct a tile with default attributes.
        /// </summary>
        public Tile()
        {
            Type = "NULL";
            Appearance = new DisplayCharacter();
            Attributes = new List<string>() { "NONE" };
            Direction = Direction.Undefined;
            DisplayDirections = new Dictionary<Direction, DisplayCharacter>();

            DisplayDirections[Direction.Undefined] = this.Appearance;
        }

        /// <summary>
        /// Construct a tile with attributes.
        /// </summary>
        /// <param name="type">The identifying name of the tile.</param>
        /// <param name="appearance">The appearance of the tile on the screen.</param>
        /// <param name="attributes">A list of any attributes on the tile.</param>
        public Tile(string type,
            DisplayCharacter appearance,
            Direction direction,
            bool blocksVision = false,
            bool blocksMovement = false,
            Dictionary<Direction, DisplayCharacter> displayDirections = null,
            List<string> attributes = null)
        {
            Type = type;
            Appearance = appearance;
            BlocksVision = blocksVision;
            BlocksMovement = blocksMovement;
            Attributes = attributes ?? new List<string>();
            Direction = direction;
            DisplayDirections = displayDirections ?? new Dictionary<Direction, DisplayCharacter>();

            DisplayDirections[Direction.Undefined] = this.Appearance;
        }

        public Tile(Tile tile) 
            : this(
                  string.Copy(tile.Type),
                  new DisplayCharacter(tile.Appearance),
                  tile.Direction,
                  tile.BlocksVision,
                  tile.BlocksMovement,
                  tile.DisplayDirections.ToDictionary(k => k.Key, v => new DisplayCharacter(v.Value)),
                  tile.Attributes.ToList())
        { }

        /// <summary>
        /// Returns a string containing the Type of the Tile.
        /// </summary>
        /// <returns>The Type of the Tile.</returns>
        public override string ToString()
        {
            return Type;
        }

        /// <summary>
        /// Gets the appearance of the tile based on the direction it is pointing.
        /// </summary>
        /// <returns>A <see cref="DisplayCharacter"/> containing the visible value.</returns>
        public DisplayCharacter GetDirectionalAppearance()
        {
            if (DisplayDirections.TryGetValue(this.Direction, out var displayCharacter))
            {
                return displayCharacter;
            }
            else
            {
                return Appearance;
            }
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        public DisplayCharacter Appearance { get; set; }

        public List<string> Attributes { get; set; }

        public string Type { get; set; }

        public bool BlocksVision = false;
        public bool BlocksMovement = false;

        public Direction Direction { get; set; }

        public Dictionary<Direction, DisplayCharacter> DisplayDirections { get; set; }

        #endregion [-- Properties --]
    }
}