﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures.AI;

namespace CavesOfTheDragonAge.Engine.Creatures
{
    public interface ICreature
    {
        ICreatureAi Ai { get; set; }
        DisplayCharacter Appearance { get; set; }
        string Identifier { get; set; }

        void TakeTurn();
    }
}