﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures.AI;
using CavesOfTheDragonAge.Engine.MagicSpells;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CavesOfTheDragonAge.Engine.Creatures
{
    /// <summary>
    /// A factory class for the Creature object, containing various creature generation functions.
    /// </summary>
    public static partial class CreatureFactory
    {
        /// <summary>
        /// Generates a Creature for a new player.
        /// </summary>
        /// <returns>A Creature for the new player.</returns>
        public static Creature GetNewPlayer(Direction facing = Direction.Undefined)
        {
            Creature player = new Creature
            {
                Identifier = GameData.Ui.PlayerId,
                Name = "Player",
                Appearance = new DisplayCharacter('@', System.Drawing.Color.White),
                Ai = new PlayerAi(),
                Health = new StatPoint(10),
                Facing = facing
            };

            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Self Heal", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("SINGLE"), MagicRuneFactory.GetNewEffectRune("HEAL")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Fireball", MagicRuneFactory.GetNewTargetRune(TargetType.LineOfSight), MagicRuneFactory.GetNewAreaRune("SMALLEXPLOSION"), MagicRuneFactory.GetNewEffectRune("FIRE")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Ice Vortex", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("MEDIUMEXPLOSION"), MagicRuneFactory.GetNewEffectRune("ICE")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Blink", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("HUGECONE"), MagicRuneFactory.GetNewEffectRune("TELEPORT")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Energy Wave", MagicRuneFactory.GetNewTargetRune(TargetType.LineOfSight), MagicRuneFactory.GetNewAreaRune("MEDIUMCONE"), MagicRuneFactory.GetNewEffectRune("FORCE")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Lightning Bolt", MagicRuneFactory.GetNewTargetRune(TargetType.Adjacent), MagicRuneFactory.GetNewAreaRune("LONGLINE"), MagicRuneFactory.GetNewEffectRune("LIGHTNING")));
            //player.SpellsKnown.Add(MagicSpellFactory.GetNewMagicSpell("Level Buster", MagicRuneFactory.GetNewTargetRune(TargetType.Self), MagicRuneFactory.GetNewAreaRune("LEVEL"), MagicRuneFactory.GetNewEffectRune("FIRE")));

            player.Ai.MyCreature = player;


            return player;
        }

        /// <summary>
        /// Generates a Creature based on the creatureType.
        /// </summary>
        /// <param name="creatureType">The type of creature to make.</param>
        /// <param name="idNum">The unique ID of the creature to make.</param>
        /// <returns>A new Creature based on the type.</returns>
        public static Creature GetNewCreature(string creatureType, int idNum, Direction facing = Direction.Undefined)
        {
            if (CreatureFormats.ContainsKey(creatureType))
            {
                var format = CreatureFormats[creatureType];
                Creature newCreature = new Creature
                {
                    Identifier = creatureType + idNum,
                    Name = format.Name,
                    Appearance = new DisplayCharacter(format.Appearance),
                    Health = new StatPoint(format.Health),
                    Mobile = format.Mobile,
                    Facing = facing,
                    DisplayDirections = format.DisplayDirections != null ? new Dictionary<Direction, DisplayCharacter>(format.DisplayDirections) : null,
                    Ai = new BruteForceAI()
                };
                newCreature.Ai.MyCreature = newCreature;
                return newCreature;
            }
            else
            {
                return new Creature();
            }
        }

        /// <summary>
        /// Load the factory with the available creature formats.
        /// </summary>
        /// <param name="creatureFormats">A list of creature formats to use.  If not sent, the creature formats
        /// will be loaded from data files.</param>
        public static void LoadCreatureFormats(Dictionary<string, CreatureFormat> creatureFormats = null)
        {
            const string CREATURE_FOLDER = @".\data\creatures\";

            JsonSerializer serializer = new JsonSerializer();
            CreatureFormats = new Dictionary<string, CreatureFormat>();

            if(creatureFormats != null)
            {
                CreatureFormats = creatureFormats;
            }
            else if (Directory.Exists(CREATURE_FOLDER))
            {
                var datafiles = Directory.GetFiles(CREATURE_FOLDER, "*.json", SearchOption.TopDirectoryOnly)
                    .Select(file => Path.GetFileName(file)).ToList();

                foreach (var file in datafiles)
                {
                    using (StreamReader sr = new StreamReader(CREATURE_FOLDER + file))
                    using (JsonReader reader = new JsonTextReader(sr))
                    {
                        serializer.Deserialize<Dictionary<string, CreatureFormat>>(reader)
                            .ToList().ForEach(item => CreatureFormats[item.Key] = item.Value);
                    }
                }
            }
        }

        private static Dictionary<string, CreatureFormat> CreatureFormats = new Dictionary<string, CreatureFormat>();
    }
}