﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Creatures
{
    public class CreatureFormat
    {
        public string Name { get; set; }

        public DisplayCharacter Appearance { get; set; }

        public int Health { get; set; }

        public bool Mobile { get; set; } = true;

        public Dictionary<Direction, DisplayCharacter> DisplayDirections { get; set; }
    }
}
