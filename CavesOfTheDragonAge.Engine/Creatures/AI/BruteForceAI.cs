﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    public class BruteForceAI : ICreatureAI
    {
        public Creature MyCreature { get; set; }

        public Instruction[] GetInstructionQueue()
        {
            if(MyCreature.Health <= 0)
            {
                var instr = new Instruction[5];
                instr[0] = InstructionFactory.GetInstruction("");
                instr[1] = InstructionFactory.GetInstruction("");
                instr[2] = InstructionFactory.GetInstruction("");
                instr[3] = InstructionFactory.GetInstruction("");
                instr[4] = InstructionFactory.GetInstruction("");

                return instr;
            }

            return GameData.DungeonMaps[GameData.CurrentDungeon].DetermineBestMove(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(MyCreature.Identifier),
                                    MyCreature.Facing, InstructionFactory.GetAvailableInstructions(MyCreature.Health));
        }

        public CreatureAction GetNextAction()
        {
            CreatureAction action = new CreatureAction(CreatureActionType.Move, (Direction)Randomization.RandomInt(2, 9));

            return action;
        }
    }
}
