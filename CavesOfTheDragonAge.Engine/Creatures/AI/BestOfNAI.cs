﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    public class BestOfNAI : ICreatureAI
    {
        public Creature MyCreature { get; set; }

        public Instruction[] GetInstructionQueue()
        {
            var availableInstructions = InstructionFactory.GetAvailableInstructions(MyCreature.Health);

            var testInstructions = new List<Tuple<Instruction[], int>>();

            for(int num = 0; num < 40; num++)
            {
                var instr = availableInstructions.GetRandomInstructions(MyCreature.QueuedInstructions, MyCreature.Health);
                var locations = GameData.DungeonMaps[GameData.CurrentDungeon].ForcastMovements(
                    GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(MyCreature.Identifier),
                    MyCreature.Facing, instr);
                var lastPostion = locations.Last().Item1;
                var distance = GameData.DungeonMaps[GameData.CurrentDungeon].MapToFlag.GoalMap(lastPostion.X, lastPostion.Y);
                if (locations.Any(loc => loc.Item3 == true && GameData.DungeonMaps[GameData.CurrentDungeon].MapToFlag.GoalMap(loc.Item1.X, loc.Item1.Y) == 0))
                {
                    distance = 0;
                }

                testInstructions.Add(new Tuple<Instruction[], int>(instr, distance));
            }

            var orderedList = testInstructions.OrderBy(i => i.Item2);

            return orderedList.First().Item1;
        }

        public CreatureAction GetNextAction()
        {
            CreatureAction action = new CreatureAction(CreatureActionType.Move, (Direction)Randomization.RandomInt(2, 9));

            return action;
        }
    }
}
