﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures.AI;
using CavesOfTheDragonAge.Engine.MagicSpells;
using System.Collections.Generic;
using System.Linq;

namespace CavesOfTheDragonAge.Engine.Creatures
{
    /// <summary>
    /// A creature to inhabit the dungeon.
    /// </summary>
    public class Creature
    {
        #region [-- Public Methods --]

        /// <summary>
        /// A default creature.
        /// </summary>
        public Creature()
        {
            Identifier = "";
            Name = "";
            Appearance = new DisplayCharacter();
            Facing = Direction.Undefined;
            DisplayDirections = new Dictionary<Direction, DisplayCharacter>();
        }

        /// <summary>
        /// Take a round of actions.
        /// </summary>
        public void LoadActions()
        {
            QueuedInstructions = Ai.GetInstructionQueue();

            //if (Health > 0 || Identifier == "PLAYER")
            //{
            //    CreatureAction action = Ai.GetNextAction();
            //    doAction(action);
            //}
        }

        /// <summary>
        /// Call for the player when they die, to end the game.
        /// </summary>
        public void OnDeath()
        {
            if (Health == 0)
            {
                GameData.DungeonMaps[GameData.CurrentDungeon].KillCreature(Identifier);
            }
        }

        /// <summary>
        /// Gets the appearance of the tile based on the direction it is pointing.
        /// </summary>
        /// <returns>A <see cref="DisplayCharacter"/> containing the visible value.</returns>
        public DisplayCharacter GetDirectionalAppearance()
        {
            if(this.Facing == Direction.Undefined)
            {
                return Appearance;
            }
            else if (DisplayDirections.TryGetValue(this.Facing, out var displayCharacter))
            {
                return displayCharacter;
            }
            else
            {
                return Appearance;
            }
        }

        /// <summary>
        /// Overload the equals operator for the Creature type.
        /// </summary>
        /// <param name="left">The Creature on the left side of the operator.</param>
        /// <param name="right">The Creature on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator ==(Creature left, Creature right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            //TODO: Add AI comparison too.
            return left.Identifier == right.Identifier && left.Name == right.Name && left.Appearance == right.Appearance;
        }

        /// <summary>
        /// Overload the not-equals operator for the Creature type.
        /// </summary>
        /// <param name="left">The Creature on the left side of the operator.</param>
        /// <param name="right">The Creature on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator !=(Creature left, Creature right)
        {
            return !(left == right);
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Perform the given action.
        /// </summary>
        /// <param name="action">The <see cref="CavesOfTheDragonAge.Common.CreatureAction"/> containing the specifics of the action to take.</param>
        private void doAction(CreatureAction action)
        {
            switch (action.ActionType)
            {
                case CreatureActionType.Move:
                    GameData.DungeonMaps[GameData.CurrentDungeon].MoveCreature(Identifier, action.ActionDirection);
                    break;

                case CreatureActionType.Open:
                    GameData.DungeonMaps[GameData.CurrentDungeon].TriggerCell(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier).GetVector(action.ActionDirection), "OPEN");
                    break;

                case CreatureActionType.Close:
                    GameData.DungeonMaps[GameData.CurrentDungeon].TriggerCell(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier).GetVector(action.ActionDirection), "CLOSE");
                    break;

                case CreatureActionType.Handle:
                    GameData.DungeonMaps[GameData.CurrentDungeon].TriggerCell(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier).GetVector(action.ActionDirection), "HANDLE");
                    break;

                case CreatureActionType.Attack:
                    GameData.DungeonMaps[GameData.CurrentDungeon].Attack(Identifier, action.ActionDirection);
                    break;

                default:
                    break;
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The identifier of the creature.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// The proper name of the creature.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The AI object controlling the creature.
        /// </summary>
        public ICreatureAI Ai { get; set; }

        /// <summary>
        /// The appearance of the creature on the screen.
        /// </summary>
        public DisplayCharacter Appearance { get; set; }

        /// <summary>
        /// The health of the creature.
        /// </summary>
        public StatPoint Health //{ get; set; }
        {
            get { return _health; }
            set
            {
                _health = value;
                if (_health == 0)
                {
                    OnDeath();
                }
            }
        }

        /// <summary>
        /// True if the creature can move, false otherwise.
        /// </summary>
        public bool Mobile { get; set; }

        private StatPoint _health;

        public int Score = 0;

        public Direction Facing { get; set; }

        public Dictionary<Direction, DisplayCharacter> DisplayDirections { get; set; }

        public Instruction[] QueuedInstructions;

        //public List<string> Attributes
        //{
        //    get { return _attributes; }
        //    set { _attributes = value; }
        //}

        //private List<string> _attributes;

        #endregion [-- Properties --]
    }
}