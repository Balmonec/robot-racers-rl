﻿using System.Drawing;

namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// Contains the information needed to display a character on the screen.
    /// </summary>
    public class DisplayCharacter
    {
        #region [-- Public Methods --]


        /// <summary>
        /// Construct a DisplayCharacter with default attributes.
        /// </summary>
        public DisplayCharacter()
        {
            Character = ' ';
            ForegroundColor = Color.Black;
            BackgroundColor = Color.Black;
        }


        /// <summary>
        /// Construct a DisplayCharacter with attributes.
        /// </summary>
        /// <param name="character">The character to display.</param>
        /// <param name="foregroundColor">The foreground color of the character.</param>
        public DisplayCharacter(char character, Color foregroundColor) : this()
        {
            Character = character;
            ForegroundColor = foregroundColor;
        }

        /// <summary>
        /// Construct a DisplayCharacter with attributes.
        /// </summary>
        /// <param name="character">The character to display.</param>
        /// <param name="foregroundColor">The foreground color of the character.</param>
        /// <param name="backgroundColor">The background color of the character.</param>
        public DisplayCharacter(char character, Color foregroundColor, Color backgroundColor) :
            this(character, foregroundColor)
        {
            BackgroundColor = backgroundColor;
        }

        /// <summary>
        /// Construct a new DisplayCharacter from another DisplayCharacter.
        /// </summary>
        /// <param name="displayCharacter">The DisplayCharacter to be copied from.</param>
        public DisplayCharacter(DisplayCharacter displayCharacter)
        {
            if(displayCharacter != null)
            {
                Character = displayCharacter.Character;
                ForegroundColor = displayCharacter.ForegroundColor;
                BackgroundColor = displayCharacter.BackgroundColor;
            }
        }

        /// <summary>
        /// Override the ToString function.
        /// </summary>
        /// <returns>A string describing the DisplayCharacter.</returns>
        public override string ToString()
        {
            return "Character(" + Character + "), ForegroundColor(" + ForegroundColor + "), BackgroundColor(" + BackgroundColor + ")";
        }

        /// <summary>
        /// Overload the equals operator for the DisplayCharacter type.
        /// </summary>
        /// <param name="left">The DisplayCharacter on the left side of the operator.</param>
        /// <param name="right">The DisplayCharacter on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator ==(DisplayCharacter left, DisplayCharacter right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.BackgroundColor == right.BackgroundColor && left.Character == right.Character && left.ForegroundColor == right.ForegroundColor;
        }

        /// <summary>
        /// Overload the not-equals operator for the DisplayCharacter type.
        /// </summary>
        /// <param name="left">The DisplayCharacter on the left side of the operator.</param>
        /// <param name="right">The DisplayCharacter on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator !=(DisplayCharacter left, DisplayCharacter right)
        {
            return !(left == right);
        }

        #endregion [-- Public Methods --]



        #region [-- Properties --]

        /// <summary>
        /// The background color of the character.
        /// </summary>
        public Color BackgroundColor { get; set; }

        /// <summary>
        /// The character to display.
        /// </summary>
        public char Character { get; set; }

        /// <summary>
        /// The foreground color of the character.
        /// </summary>
        public Color ForegroundColor { get; set; }

        #endregion [-- Properties --]
    }
}