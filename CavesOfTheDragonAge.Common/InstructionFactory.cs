﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Common
{
    public static class InstructionFactory
    {
        static InstructionFactory()
        {
            const string INSTRUCTION_FOLDER = @".\data\instructions\";

            JsonSerializer serializer = new JsonSerializer();
            serializer.Formatting = Formatting.Indented;
            InstructionFormats = new Dictionary<string, Instruction>();

            var datafiles = Directory.GetFiles(INSTRUCTION_FOLDER, "*.json", SearchOption.TopDirectoryOnly)
                .Select(file => Path.GetFileName(file)).ToList();

            foreach (var file in datafiles)
            {
                using (StreamReader sr = new StreamReader(INSTRUCTION_FOLDER + file))
                using (JsonReader reader = new JsonTextReader(sr))
                {
                    serializer.Deserialize<Dictionary<string, Instruction>>(reader)
                        .ToList().ForEach(item => InstructionFormats[item.Key] = item.Value);
                }
            }
        }

        public static Instruction GetInstruction(string identifier)
        {
            if (InstructionFormats.ContainsKey(identifier))
            {
                return InstructionFormats[identifier].Copy();
            }
            else
            {
                return new Instruction("", 0, 0, null, null, InstructionType.Undefined, 0);
            }
        }

        public static List<Tuple<Instruction, int>> GetAvailableInstructions(int numberInstructions = 9)
        {
            var availableInstructions = new List<string>();

            for(int i = 0; i < numberInstructions; i++)
            {
                availableInstructions.Add(InstructionFormats.Keys.ToList()[Randomization.RandomInt(0, InstructionFormats.Count - 1)]);
            }

            return InstructionFormats.Values.OrderBy(ins => ins.ListOrder)
                .Select(ins => new Tuple<Instruction, int>(ins, availableInstructions.Count(i => ins.Identifier == i))).ToList();
        }

        private static Dictionary<string, Instruction> InstructionFormats = new Dictionary<string, Instruction>();

        public static int InstructionsRemaining(this List<Tuple<Instruction, int>> availableInstructions, Instruction[] usingQueue, string identifier)
        {
            var available = availableInstructions.First(i => i.Item1.Identifier == identifier)?.Item2 ?? 0;
            var used = usingQueue.Count(i => i?.Identifier == identifier);
            return available - used;
        }

        public static Instruction[] GetRandomInstructions(this List<Tuple<Instruction, int>> availableInstructions, Instruction[] oldQueue, int health)
        {
            health = health < 0 ? 0 : health;
            var instructions = new Instruction[5];

            for (int i = 0; i < (health < 5 ? health : 5); i++)
            {
                var unusedInstructions = availableInstructions.Where(inst => availableInstructions.InstructionsRemaining(instructions, inst.Item1.Identifier) > 0).ToList();
                if (unusedInstructions.Count > 0)
                {
                    instructions[i] = GetInstruction(unusedInstructions[Randomization.RandomIndex(unusedInstructions.Count)].Item1.Identifier);
                }
                else
                {
                    instructions[i] = null;
                }
            }

            for (int i = health; i < 5; i++)
            {
                instructions[i] = GetInstruction(oldQueue[i].Identifier);
            }

            return instructions;
        }
    }
}
