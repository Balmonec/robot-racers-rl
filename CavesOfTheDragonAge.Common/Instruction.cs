﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Common
{
    public class Instruction
    {
        public Instruction(
            string identifier,
            int priority,
            int listOrder,
            DisplayCharacter[,] cardView,
            DisplayCharacter[] listView,
            InstructionType instructionCode,
            int magnitude)
        {
            Identifier = identifier;
            Priority = priority;
            ListOrder = listOrder;
            CardView = cardView;
            ListView = listView;
            InstructionCode = instructionCode;
            Magnitude = magnitude;
        }

        public string Identifier { get; }

        public int Priority { get; }

        public int ListOrder { get; }

        public DisplayCharacter[,] CardView { get; }

        public DisplayCharacter [] ListView { get; }

        public InstructionType InstructionCode { get; }

        public int Magnitude { get; }

        public Instruction Copy()
        {
            return new Instruction(
            Identifier,
            Priority,
            ListOrder,
            CardView,
            ListView,
            InstructionCode,
            Magnitude);
        }

        /// <summary>
        /// Get the movements indicated by the instruction.
        /// </summary>
        /// <returns>
        /// A list of movements, of which the first Direction indicates the direction to move
        /// and the second Direction indicates the facing after the movment.  Both directions
        /// are relative, with North as the current facing.
        /// </returns>
        public List<Tuple<Direction, Direction>> GetVectors()
        {
            var vectors = new List<Tuple<Direction, Direction>>();

            switch (InstructionCode)
            {
                case InstructionType.MoveForward:
                    {
                        for (int i = 0; i < Magnitude; i++)
                        {
                            vectors.Add(new Tuple<Direction, Direction>(Direction.North, Direction.North));
                        }
                    }
                    break;

                case InstructionType.MoveBackward:
                    {
                        for (int i = 0; i < Magnitude; i++)
                        {
                            vectors.Add(new Tuple<Direction, Direction>(Direction.South, Direction.North));
                        }
                    }
                    break;

                case InstructionType.TurnLeft:
                    vectors.Add(new Tuple<Direction, Direction>(Direction.Here, Direction.North.Left90()));
                    break;

                case InstructionType.TurnRight:
                    vectors.Add(new Tuple<Direction, Direction>(Direction.Here, Direction.North.Right90()));
                    break;

                case InstructionType.TurnAround:
                    vectors.Add(new Tuple<Direction, Direction>(Direction.Here, Direction.North.Opposite()));
                    break;
            }

            return vectors;
        }
    }

    public enum InstructionType
    {
        Undefined,
        MoveForward,
        MoveBackward,
        TurnRight,
        TurnLeft,
        TurnAround
    }
}
