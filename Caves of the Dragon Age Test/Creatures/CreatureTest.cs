﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.Creatures.AI;

namespace CavesOfTheDragonAgeTests
{
    
    
    /// <summary>
    ///This is a test class for CreatureTest and is intended
    ///to contain all CreatureTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CreatureTest
    {
        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            GameData.Setup(new UserInterfaceStub());
        }


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for Creature Constructor
        ///</summary>
        [TestMethod()]
        public void CreatureConstructorTest()
        {
            Creature target = new Creature();
            Assert.AreEqual("", target.Identifier);
            Assert.AreEqual("", target.Name);
            Assert.AreEqual(new DisplayCharacter().ToString(), target.Appearance.ToString());
        }

        /// <summary>
        ///A test for op_Equality
        ///</summary>
        [TestMethod()]
        public void op_EqualityTest()
        {
            Creature left = CreatureFactory.GetNewPlayer();
            Creature right = CreatureFactory.GetNewPlayer();
            bool expected = true;
            bool actual;
            actual = (left == right);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for op_Inequality
        ///</summary>
        [TestMethod()]
        public void op_InequalityTest()
        {
            Creature left = CreatureFactory.GetNewPlayer();
            Creature right = CreatureFactory.GetNewPlayer();
            bool expected = false;
            bool actual;
            actual = (left != right);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Appearance
        ///</summary>
        [TestMethod()]
        public void AppearanceTest()
        {
            var sampleData = new[]
            {
                new { DispChar = new DisplayCharacter()},
                new { DispChar = new DisplayCharacter('a', GameColors.Blue)},
                new { DispChar = new DisplayCharacter('b', GameColors.Brown, GameColors.Blue)},
            };
            foreach (var item in sampleData)
            {
                Creature target = new Creature();
                DisplayCharacter expected = item.DispChar;
                DisplayCharacter actual;
                target.Appearance = expected;
                actual = target.Appearance;
                Assert.AreEqual(expected, actual, "Appearance: " + item.DispChar);
            }
        }

        /// <summary>
        ///A test for Identifier
        ///</summary>
        [TestMethod()]
        public void IdentifierTest()
        {
            var sampleData = new[]
            {
                new { Name = "Hi"},
                new { Name = "Lo"},
                new { Name = "To"},
                new { Name = "Slow"},
            };
            foreach (var item in sampleData)
            {
                Creature target = new Creature();
                string expected = item.Name;
                string actual;
                target.Identifier = expected;
                actual = target.Identifier;
                Assert.AreEqual(expected, actual, "Identifier: " + item.Name);
            }
        }

        /// <summary>
        ///A test for Name
        ///</summary>
        [TestMethod()]
        public void NameTest()
        {
            var sampleData = new[]
            {
                new { Name = "Hi"},
                new { Name = "Lo"},
                new { Name = "To"},
                new { Name = "Slow"},
            };
            foreach (var item in sampleData)
            {
                Creature target = new Creature();
                string expected = item.Name;
                string actual;
                target.Name = expected;
                actual = target.Name;
                Assert.AreEqual(expected, actual, "Name: " + item.Name);
            }
        }

        /// <summary>
        ///A test for Ai
        ///</summary>
        [TestMethod()]
        public void AiTest()
        {
            var sampleData = new[]
            {
                new { AI = new PlayerAi()},
            };
            foreach (var item in sampleData)
            {
                Creature target = new Creature();
                ICreatureAI expected = item.AI;
                ICreatureAI actual;
                target.Ai = expected;
                actual = target.Ai;
                Assert.AreEqual(expected, actual, "AI: " + item.AI);
            }
        }

        /// <summary>
        ///A test for Health
        ///</summary>
        [TestMethod()]
        public void HealthTest()
        {
            var sampleData = new[]
            {
                new { Health = new StatPoint(100)},
            };
            foreach (var item in sampleData)
            {
                Creature target = new Creature();
                StatPoint expected = item.Health;
                StatPoint actual;
                target.Health = expected;
                actual = target.Health;
                Assert.AreEqual(expected, actual, "Health: " + item.Health);
            }
        }
    }
}
