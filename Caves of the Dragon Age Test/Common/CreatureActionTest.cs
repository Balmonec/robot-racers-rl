﻿using CavesOfTheDragonAge.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CavesOfTheDragonAgeTests
{
    [TestClass]
    public class CreatureActionTest
    {
        ///// <summary>
        ///// Tests that the Validate method returns false when the ActionType is Unknown.
        ///// </summary>
        //[TestMethod]
        //public void ValidateReturnsFalseWhenActionTypeIsUndefinedTest()
        //{
        //    //Arrange
        //    CreatureAction creatureAction = new CreatureAction();
        //    creatureAction.ActionType = CreatureActionType.Undefined;

        //    //Act
        //    bool actual = creatureAction.Validate();

        //    //Assert
        //    const bool EXPECTED = false;
        //    Assert.AreEqual(EXPECTED, actual);
        //}

        ///// <summary>
        ///// Tests that the Validate method returns true when the ActionType is Wait.
        ///// </summary>
        //[TestMethod]
        //public void ValidateReturnsTrueWhenActionTypeIsWaitTest()
        //{
        //    //Arrange
        //    CreatureAction creatureAction = new CreatureAction();
        //    creatureAction.ActionType = CreatureActionType.Wait;

        //    //Act
        //    bool actual = creatureAction.Validate();

        //    //Assert
        //    const bool EXPECTED = true;
        //    Assert.AreEqual(EXPECTED, actual);
        //}

        ///// <summary>
        ///// Tests that the Validate method returns true when the ActionType is Move
        ///// and the Direction is not Undefined.
        ///// </summary>
        //[TestMethod]
        //public void ValidateReturnsTrueWhenActionTypeIsMoveAndDirectionIsNotUndefinedTest()
        //{
        //    //Arrange
        //    CreatureAction creatureAction = new CreatureAction();
        //    creatureAction.ActionType = CreatureActionType.Move;
        //    creatureAction.ActionDirection = Direction.North;

        //    //Act
        //    bool actual = creatureAction.Validate();

        //    //Assert
        //    const bool EXPECTED = true;
        //    Assert.AreEqual(EXPECTED, actual);
        //}

        ///// <summary>
        ///// Tests that the Validate method returns false when the ActionType is Move
        ///// and the Direction is Undefined.
        ///// </summary>
        //[TestMethod]
        //public void ValidateReturnsFalseWhenActionTypeIsMoveAndDirectionIsUndefinedTest()
        //{
        //    //Arrange
        //    CreatureAction creatureAction = new CreatureAction();
        //    creatureAction.ActionType = CreatureActionType.Move;

        //    //Act
        //    bool actual = creatureAction.Validate();

        //    //Assert
        //    const bool EXPECTED = false;
        //    Assert.AreEqual(EXPECTED, actual);
        //}

        /// <summary>
        /// Tests the Validate method when the ActionType is Close.
        /// </summary>
        [TestMethod]
        public void ValidateCloseTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, true),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Close, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Handle.
        /// </summary>
        [TestMethod]
        public void ValidateHandleTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, true),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Handle, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Move.
        /// </summary>
        [TestMethod]
        public void ValidateMoveTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, true),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, true),
                                                    new Tuple<Direction, bool>(Direction.Down, true),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Move, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Open.
        /// </summary>
        [TestMethod]
        public void ValidateOpenTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, true),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Open, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Undefined.
        /// </summary>
        [TestMethod]
        public void ValidateUndefinedTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, false),
                                                    new Tuple<Direction, bool>(Direction.North, false),
                                                    new Tuple<Direction, bool>(Direction.South, false),
                                                    new Tuple<Direction, bool>(Direction.East, false),
                                                    new Tuple<Direction, bool>(Direction.West, false),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, false),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, false),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, false),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, false),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Undefined, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Waiting.
        /// </summary>
        [TestMethod]
        public void ValidateWaitingTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, true),
                                                    new Tuple<Direction, bool>(Direction.Here, true),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, true),
                                                    new Tuple<Direction, bool>(Direction.Down, true),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Waiting, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Wait.
        /// </summary>
        [TestMethod]
        public void ValidateWaitTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, true),
                                                    new Tuple<Direction, bool>(Direction.Here, true),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, true),
                                                    new Tuple<Direction, bool>(Direction.Down, true),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Wait, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        /// Tests the Validate method when the ActionType is Walk.
        /// </summary>
        [TestMethod]
        public void ValidateWalkTest()
        {
            Tuple<Direction, bool>[] sampleData = { new Tuple<Direction, bool>(Direction.Undefined, false),
                                                    new Tuple<Direction, bool>(Direction.Here, false),
                                                    new Tuple<Direction, bool>(Direction.North, true),
                                                    new Tuple<Direction, bool>(Direction.South, true),
                                                    new Tuple<Direction, bool>(Direction.East, true),
                                                    new Tuple<Direction, bool>(Direction.West, true),
                                                    new Tuple<Direction, bool>(Direction.NorthEast, true),
                                                    new Tuple<Direction, bool>(Direction.NorthWest, true),
                                                    new Tuple<Direction, bool>(Direction.SouthEast, true),
                                                    new Tuple<Direction, bool>(Direction.SouthWest, true),
                                                    new Tuple<Direction, bool>(Direction.Up, false),
                                                    new Tuple<Direction, bool>(Direction.Down, false),
                                                    };

            foreach (Tuple<Direction, bool> currentData in sampleData)
            {
                CreatureAction testAction = new CreatureAction(CreatureActionType.Walk, currentData.Item1);
                bool expected = currentData.Item2;
                bool actual = testAction.Validate();
                Assert.AreEqual(expected, actual, "Direction<" + currentData.Item1 + ">");
            }
        }
    }
}