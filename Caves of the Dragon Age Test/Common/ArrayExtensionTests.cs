﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CavesOfTheDragonAge.Common;
using System;

namespace CavesOfTheDragonAge.Common.Tests
{
    [TestClass()]
    public class ArrayExtensionTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes

        [TestMethod()]
        public void IndexInBounds2DTestXYString()
        {
            var sampleData = new[]
            {
                new { Array = new string[5,5], X = 0, Y = 0, Expected = true },
                new { Array = new string[5,5], X = -1, Y = 0, Expected = false },
                new { Array = new string[5,5], X = 0, Y = -1, Expected = false },
                new { Array = new string[5,5], X = -1, Y = -1, Expected = false },
                new { Array = new string[5,5], X = 4, Y = 4, Expected = true },
                new { Array = new string[5,5], X = 5, Y = 4, Expected = false },
                new { Array = new string[5,5], X = 4, Y = 5, Expected = false },
                new { Array = new string[5,5], X = 5, Y = 5, Expected = false },
                new { Array = new string[10,5], X = 0, Y = 0, Expected = true },
                new { Array = new string[10,5], X = -1, Y = 0, Expected = false },
                new { Array = new string[10,5], X = 0, Y = -1, Expected = false },
                new { Array = new string[10,5], X = -1, Y = -1, Expected = false },
                new { Array = new string[10,5], X = 9, Y = 4, Expected = true },
                new { Array = new string[10,5], X = 10, Y = 4, Expected = false },
                new { Array = new string[10,5], X = 4, Y = 5, Expected = false },
                new { Array = new string[10,5], X = 10, Y = 5, Expected = false },
                new { Array = new string[5,10], X = 0, Y = 0, Expected = true },
                new { Array = new string[5,10], X = -1, Y = 0, Expected = false },
                new { Array = new string[5,10], X = 0, Y = -1, Expected = false },
                new { Array = new string[5,10], X = -1, Y = -1, Expected = false },
                new { Array = new string[5,10], X = 4, Y = 9, Expected = true },
                new { Array = new string[5,10], X = 5, Y = 9, Expected = false },
                new { Array = new string[5,10], X = 4, Y = 10, Expected = false },
                new { Array = new string[5,10], X = 5, Y = 10, Expected = false },
            };

            foreach (var item in sampleData)
            {
                bool expected = item.Expected;

                bool actual = item.Array.IndexInBounds2D(item.X, item.Y);

                Assert.AreEqual(expected, actual, "Dimension<" + item.Array.GetUpperBound(0) + ", " + item.Array.GetUpperBound(1) + ">. X<" + item.X + ">. Y<" + item.Y + ">.");
            }
        }

        [TestMethod()]
        public void IndexInBounds2DTestXYInt()
        {
            var sampleData = new[]
            {
                new { Array = new int[5,5], X = 0, Y = 0, Expected = true },
                new { Array = new int[5,5], X = -1, Y = 0, Expected = false },
                new { Array = new int[5,5], X = 0, Y = -1, Expected = false },
                new { Array = new int[5,5], X = -1, Y = -1, Expected = false },
                new { Array = new int[5,5], X = 4, Y = 4, Expected = true },
                new { Array = new int[5,5], X = 5, Y = 4, Expected = false },
                new { Array = new int[5,5], X = 4, Y = 5, Expected = false },
                new { Array = new int[5,5], X = 5, Y = 5, Expected = false },
                new { Array = new int[10,5], X = 0, Y = 0, Expected = true },
                new { Array = new int[10,5], X = -1, Y = 0, Expected = false },
                new { Array = new int[10,5], X = 0, Y = -1, Expected = false },
                new { Array = new int[10,5], X = -1, Y = -1, Expected = false },
                new { Array = new int[10,5], X = 9, Y = 4, Expected = true },
                new { Array = new int[10,5], X = 10, Y = 4, Expected = false },
                new { Array = new int[10,5], X = 9, Y = 5, Expected = false },
                new { Array = new int[10,5], X = 10, Y = 5, Expected = false },
                new { Array = new int[5,10], X = 0, Y = 0, Expected = true },
                new { Array = new int[5,10], X = -1, Y = 0, Expected = false },
                new { Array = new int[5,10], X = 0, Y = -1, Expected = false },
                new { Array = new int[5,10], X = -1, Y = -1, Expected = false },
                new { Array = new int[5,10], X = 4, Y = 9, Expected = true },
                new { Array = new int[5,10], X = 5, Y = 9, Expected = false },
                new { Array = new int[5,10], X = 4, Y = 10, Expected = false },
                new { Array = new int[5,10], X = 5, Y = 10, Expected = false },
            };

            foreach (var item in sampleData)
            {
                bool expected = item.Expected;

                bool actual = item.Array.IndexInBounds2D(item.X, item.Y);

                Assert.AreEqual(expected, actual, "Dimension<" + item.Array.GetUpperBound(0) + ", " + item.Array.GetUpperBound(1) + ">. X<" + item.X + ">. Y<" + item.Y + ">.");
            }
        }

        [TestMethod()]
        public void IndexInBounds2DTestXYRank()
        {
            var sampleData = new[]
            {
                new { Rank = 1, Type = typeof(string), X = 0, Y = 0, Expected = false },
                new { Rank = 2, Type = typeof(string), X = 0, Y = 0, Expected = true },
                new { Rank = 3, Type = typeof(string), X = 0, Y = 0, Expected = false },
                new { Rank = 1, Type = typeof(int), X = 0, Y = 0, Expected = false },
                new { Rank = 2, Type = typeof(int), X = 0, Y = 0, Expected = true },
                new { Rank = 3, Type = typeof(int), X = 0, Y = 0, Expected = false },
            };

            foreach (var item in sampleData)
            {
                int[] bounds = new int[item.Rank];

                for (int i = 0; i < item.Rank; i++)
                {
                    bounds[i] = 2;
                }

                Array array = Array.CreateInstance(item.Type, bounds);

                bool expected = item.Expected;

                bool actual = array.IndexInBounds2D(item.X, item.Y);

                Assert.AreEqual(expected, actual, "Rank<" + item.Rank + ">. Type<" + item.Type + ">.");
            }
        }

        [TestMethod()]
        public void IndexInBounds2DTestLocationString()
        {
            var sampleData = new[]
            {
                new { Array = new string[5,5], X = 0, Y = 0, Expected = true },
                new { Array = new string[5,5], X = -1, Y = 0, Expected = false },
                new { Array = new string[5,5], X = 0, Y = -1, Expected = false },
                new { Array = new string[5,5], X = -1, Y = -1, Expected = false },
                new { Array = new string[5,5], X = 4, Y = 4, Expected = true },
                new { Array = new string[5,5], X = 5, Y = 4, Expected = false },
                new { Array = new string[5,5], X = 4, Y = 5, Expected = false },
                new { Array = new string[5,5], X = 5, Y = 5, Expected = false },
                new { Array = new string[10,5], X = 0, Y = 0, Expected = true },
                new { Array = new string[10,5], X = -1, Y = 0, Expected = false },
                new { Array = new string[10,5], X = 0, Y = -1, Expected = false },
                new { Array = new string[10,5], X = -1, Y = -1, Expected = false },
                new { Array = new string[10,5], X = 9, Y = 4, Expected = true },
                new { Array = new string[10,5], X = 10, Y = 4, Expected = false },
                new { Array = new string[10,5], X = 9, Y = 5, Expected = false },
                new { Array = new string[10,5], X = 10, Y = 5, Expected = false },
                new { Array = new string[5,10], X = 0, Y = 0, Expected = true },
                new { Array = new string[5,10], X = -1, Y = 0, Expected = false },
                new { Array = new string[5,10], X = 0, Y = -1, Expected = false },
                new { Array = new string[5,10], X = -1, Y = -1, Expected = false },
                new { Array = new string[5,10], X = 4, Y = 9, Expected = true },
                new { Array = new string[5,10], X = 5, Y = 9, Expected = false },
                new { Array = new string[5,10], X = 4, Y = 10, Expected = false },
                new { Array = new string[5,10], X = 5, Y = 10, Expected = false },
            };

            foreach (var item in sampleData)
            {
                bool expected = item.Expected;

                Location location = new Location(item.X, item.Y);

                bool actual = item.Array.IndexInBounds2D(location);

                Assert.AreEqual(expected, actual, "Dimension<" + item.Array.GetUpperBound(0) + ", " + item.Array.GetUpperBound(1) + ">. Location<" + location + ">.");
            }
        }

        [TestMethod()]
        public void IndexInBounds2DTestLocationInt()
        {
            var sampleData = new[]
            {
                new { Array = new int[5,5], X = 0, Y = 0, Expected = true },
                new { Array = new int[5,5], X = -1, Y = 0, Expected = false },
                new { Array = new int[5,5], X = 0, Y = -1, Expected = false },
                new { Array = new int[5,5], X = -1, Y = -1, Expected = false },
                new { Array = new int[5,5], X = 4, Y = 4, Expected = true },
                new { Array = new int[5,5], X = 5, Y = 4, Expected = false },
                new { Array = new int[5,5], X = 4, Y = 5, Expected = false },
                new { Array = new int[5,5], X = 5, Y = 5, Expected = false },
                new { Array = new int[10,5], X = 0, Y = 0, Expected = true },
                new { Array = new int[10,5], X = -1, Y = 0, Expected = false },
                new { Array = new int[10,5], X = 0, Y = -1, Expected = false },
                new { Array = new int[10,5], X = -1, Y = -1, Expected = false },
                new { Array = new int[10,5], X = 9, Y = 4, Expected = true },
                new { Array = new int[10,5], X = 10, Y = 4, Expected = false },
                new { Array = new int[10,5], X = 9, Y = 5, Expected = false },
                new { Array = new int[10,5], X = 10, Y = 5, Expected = false },
                new { Array = new int[5,10], X = 0, Y = 0, Expected = true },
                new { Array = new int[5,10], X = -1, Y = 0, Expected = false },
                new { Array = new int[5,10], X = 0, Y = -1, Expected = false },
                new { Array = new int[5,10], X = -1, Y = -1, Expected = false },
                new { Array = new int[5,10], X = 4, Y = 9, Expected = true },
                new { Array = new int[5,10], X = 5, Y = 9, Expected = false },
                new { Array = new int[5,10], X = 4, Y = 10, Expected = false },
                new { Array = new int[5,10], X = 5, Y = 10, Expected = false },
            };

            foreach (var item in sampleData)
            {
                bool expected = item.Expected;

                Location location = new Location(item.X, item.Y);

                bool actual = item.Array.IndexInBounds2D(location);

                Assert.AreEqual(expected, actual, "Dimension<" + item.Array.GetUpperBound(0) + ", " + item.Array.GetUpperBound(1) + ">. Location<" + location + ">.");
            }
        }

        [TestMethod()]
        public void IndexInBounds2DTestLocationRank()
        {
            var sampleData = new[]
            {
                new { Rank = 1, Type = typeof(string), X = 0, Y = 0, Expected = false },
                new { Rank = 2, Type = typeof(string), X = 0, Y = 0, Expected = true },
                new { Rank = 3, Type = typeof(string), X = 0, Y = 0, Expected = false },
                new { Rank = 1, Type = typeof(int), X = 0, Y = 0, Expected = false },
                new { Rank = 2, Type = typeof(int), X = 0, Y = 0, Expected = true },
                new { Rank = 3, Type = typeof(int), X = 0, Y = 0, Expected = false },
            };

            foreach (var item in sampleData)
            {
                int[] bounds = new int[item.Rank];

                for (int i = 0; i < item.Rank; i++)
                {
                    bounds[i] = 2;
                }

                Array array = Array.CreateInstance(item.Type, bounds);

                bool expected = item.Expected;

                Location location = new Location(item.X, item.Y);

                bool actual = array.IndexInBounds2D(location);

                Assert.AreEqual(expected, actual, "Rank<" + item.Rank + ">. Type<" + item.Type + ">.");
            }
        }

        [TestMethod()]
        public void Init1DTestString()
        {
            var sampleData = new[]
            {
                new { Length = 1, Value = "Hi" },
                new { Length = 5, Value = "Hi" },
                new { Length = 5, Value = "Pie" },
            };

            foreach (var item in sampleData)
            {
                string[] array = new string[item.Length];

                array.Init1D(item.Value);

                for(int i=0; i<item.Length; i++)
                {
                    string expected = item.Value;
                    string actual = array[i];
                    Assert.AreEqual(expected, actual, "Length<" + item.Length + ">. i<" + i + ">.");
                }
            }
        }

        [TestMethod()]
        public void Init1DTestInt()
        {
            var sampleData = new[]
            {
                new { Length = 1, Value = 5 },
                new { Length = 5, Value = -15 },
                new { Length = 5, Value = int.MaxValue },
            };

            foreach (var item in sampleData)
            {
                int[] array = new int[item.Length];

                array.Init1D(item.Value);

                for (int i = 0; i < item.Length; i++)
                {
                    int expected = item.Value;
                    int actual = array[i];
                    Assert.AreEqual(expected, actual, "Length<" + item.Length + ">. i<" + i + ">.");
                }
            }
        }

        [TestMethod()]
        public void Init2DTestString()
        {
            var sampleData = new[]
            {
                new { Size = new Tuple<int, int>(1, 1), Value = "Hi" },
                new { Size = new Tuple<int, int>(5, 1), Value = "Hi" },
                new { Size = new Tuple<int, int>(1, 5), Value = "Pie" },
                new { Size = new Tuple<int, int>(100, 50), Value = "Pie" },
            };

            foreach (var item in sampleData)
            {
                string[,] array = new string[item.Size.Item1, item.Size.Item2];

                array.Init2D(item.Value);

                for (int x = 0; x < item.Size.Item1; x++)
                {
                    for (int y = 0; y < item.Size.Item2; y++)
                    {
                        string expected = item.Value;
                        string actual = array[x, y];
                        Assert.AreEqual(expected, actual, "Size<" + item.Size + ">. Position<" + x + ", " + y + ">.");
                    }
                }
            }
        }

        [TestMethod()]
        public void Init2DTestInt()
        {
            var sampleData = new[]
            {
                new { Size = new Tuple<int, int>(1, 1), Value = 1 },
                new { Size = new Tuple<int, int>(5, 1), Value = 5 },
                new { Size = new Tuple<int, int>(1, 5), Value = 27 },
                new { Size = new Tuple<int, int>(100, 50), Value = int.MaxValue },
            };

            foreach (var item in sampleData)
            {
                int[,] array = new int[item.Size.Item1, item.Size.Item2];

                array.Init2D(item.Value);

                for (int x = 0; x < item.Size.Item1; x++)
                {
                    for (int y = 0; y < item.Size.Item2; y++)
                    {
                        int expected = item.Value;
                        int actual = array[x, y];
                        Assert.AreEqual(expected, actual, "Size<" + item.Size + ">. Position<" + x + ", " + y + ">.");
                    }
                }
            }
        }
    }
}