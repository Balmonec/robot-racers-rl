﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Common.Tests
{
    [TestClass()]
    public class UtilityTests
    {
        [TestMethod()]
        public void ClampTestInt()
        {
            var sampleData = new[]
            {
                new { Row = 0, Value = 0, Minimum = 0, Maximum = 0, Expected = 0 },
                new { Row = 1, Value = 1, Minimum = 0, Maximum = 0, Expected = 0 },
                new { Row = 2, Value = -1, Minimum = 0, Maximum = 0, Expected = 0 },
                new { Row = 3, Value = 0, Minimum = 0, Maximum = 10, Expected = 0 },
                new { Row = 4, Value = 10, Minimum = 0, Maximum = 10, Expected = 10 },
                new { Row = 5, Value = 5, Minimum = 0, Maximum = 10, Expected = 5 },
                new { Row = 6, Value = -1, Minimum = 0, Maximum = 10, Expected = 0 },
                new { Row = 7, Value = 11, Minimum = 0, Maximum = 10, Expected = 10 },

                new { Row = 10, Value = 0, Minimum = 1, Maximum = -1, Expected = 0 },
                new { Row = 11, Value = 1, Minimum = 0, Maximum = 0, Expected = 0 },
                new { Row = 12, Value = -1, Minimum = 0, Maximum = 0, Expected = 0 },
                new { Row = 13, Value = 0, Minimum = 10, Maximum = 0, Expected = 0 },
                new { Row = 14, Value = 10, Minimum = 10, Maximum = 0, Expected = 10 },
                new { Row = 15, Value = 5, Minimum = 10, Maximum = 0, Expected = 5 },
                new { Row = 16, Value = -1, Minimum = 10, Maximum = 0, Expected = 0 },
                new { Row = 17, Value = 11, Minimum = 10, Maximum = 0, Expected = 10 },
            };
            foreach (var item in sampleData)
            {
                int actual = Utility.Clamp(item.Value, item.Minimum, item.Maximum);

                Assert.AreEqual(item.Expected, actual,
                    "Row " + item.Row + ". Min:<" + item.Minimum + ">. Max:<" + item.Maximum + ">.");
            }
        }
    }
}