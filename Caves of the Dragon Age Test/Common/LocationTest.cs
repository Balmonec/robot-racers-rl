﻿using CavesOfTheDragonAge.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CavesOfTheDragonAgeTests
{
    /// <summary>
    ///This is a test class for LocationTest and is intended
    ///to contain all LocationTest Unit Tests
    ///</summary>
    [TestClass()]
    public class LocationTest
    {
        /// <summary>
        ///A test for GetVector
        ///</summary>
        [TestMethod()]
        public void GetVectorTest()
        {
            Tuple<Location, Direction, Location>[] sampleData = { new Tuple<Location, Direction, Location>(new Location(0, 0), Direction.North, new Location(0, -1)),
                                                                  new Tuple<Location, Direction, Location>(new Location(2, 0), Direction.North, new Location(2, -1)),
                                                                  new Tuple<Location, Direction, Location>(new Location(7, 9), Direction.SouthEast, new Location(8, 10)),
                                                                  new Tuple<Location, Direction, Location>(new Location(3, 9), Direction.West, new Location(2, 9)),
                                                                  new Tuple<Location, Direction, Location>(new Location(4, 1), Direction.Down, new Location(4, 1)),
                                                                  };
            foreach (Tuple<Location, Direction, Location> currentData in sampleData)
            {
                Location expected = currentData.Item3;
                Location actual = currentData.Item1.GetVector(currentData.Item2);
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">. Direction<" + currentData.Item2 + ">");
            }
        }

        /// <summary>
        ///A test for GetDirectionTowardsClockwise
        ///</summary>
        [TestMethod()]
        public void GetDirectionTowardsClockwiseTest()
        {
            var sampleData = new[]
            {
                new { Origin = new Location(2, 2), Destination = new Location(2, 2), Expected = Direction.Undefined },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.North), Expected = Direction.North },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.South), Expected = Direction.South },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.East), Expected = Direction.East },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.West), Expected = Direction.West },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.NorthEast), Expected = Direction.NorthEast },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.NorthWest), Expected = Direction.NorthWest },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.SouthEast), Expected = Direction.SouthEast },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.SouthWest), Expected = Direction.SouthWest },
                new { Origin = new Location(2, 2), Destination = new Location(3, 0), Expected = Direction.NorthEast },
            };
            foreach (var item in sampleData)
            {
                Direction expected = item.Expected;
                Direction actual = item.Origin.GetDirectionTowardsClockwise(item.Destination);
                Assert.AreEqual(expected, actual, "Origin<" + item.Origin + ">. Destination<" + item.Destination + ">");

                expected = item.Expected;
                actual = item.Origin.GetDirectionTowardsClockwise(item.Destination.X, item.Destination.Y);
                Assert.AreEqual(expected, actual, "Origin<" + item.Origin + ">. Destination<" + item.Destination + ">");
            }
        }

        /// <summary>
        ///A test for GetDirectionTowardsCounterclockwise
        ///</summary>
        [TestMethod()]
        public void GetDirectionTowardsCounterclockwiseTest()
        {
            var sampleData = new[]
            {
                new { Origin = new Location(2, 2), Destination = new Location(2, 2), Expected = Direction.Undefined },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.North), Expected = Direction.North },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.South), Expected = Direction.South },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.East), Expected = Direction.East },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.West), Expected = Direction.West },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.NorthEast), Expected = Direction.NorthEast },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.NorthWest), Expected = Direction.NorthWest },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.SouthEast), Expected = Direction.SouthEast },
                new { Origin = new Location(2, 2), Destination = new Location(2, 2).GetVector(Direction.SouthWest), Expected = Direction.SouthWest },
                new { Origin = new Location(2, 2), Destination = new Location(3, 0), Expected = Direction.North },
            };
            foreach (var item in sampleData)
            {
                Direction expected = item.Expected;
                Direction actual = item.Origin.GetDirectionTowardsCounterclockwise(item.Destination);
                Assert.AreEqual(expected, actual, "Origin<" + item.Origin + ">. Destination<" + item.Destination + ">");

                expected = item.Expected;
                actual = item.Origin.GetDirectionTowardsCounterclockwise(item.Destination.X, item.Destination.Y);
                Assert.AreEqual(expected, actual, "Origin<" + item.Origin + ">. Destination<" + item.Destination + ">");
            }
        }

        /// <summary>
        ///A test for op_Addition
        ///</summary>
        [TestMethod()]
        public void op_AdditionTest()
        {
            Tuple<Location, Location, Location>[] sampleData = { new Tuple<Location, Location, Location>(new Location(0, 0), new Location(1, 1), new Location(1, 1)),
                                                                 new Tuple<Location, Location, Location>(new Location(0, 0), new Location(0, 0), new Location(0, 0)),
                                                                 new Tuple<Location, Location, Location>(new Location(1, 1), new Location(0, 0), new Location(1, 1)),
                                                                 new Tuple<Location, Location, Location>(new Location(1, -1), new Location(1, 1), new Location(2, 0)),
                                                                 new Tuple<Location, Location, Location>(new Location(100, 0), new Location(-50, 1), new Location(50, 1)),
                                                                 new Tuple<Location, Location, Location>(new Location(6, -5), new Location(1, 1), new Location(7, -4)),
                                                                 new Tuple<Location, Location, Location>(new Location(1, 1), new Location(1, 1), new Location(2, 2)),
                                                                 };
            foreach (Tuple<Location, Location, Location> currentData in sampleData)
            {
                Location expected = currentData.Item3;
                Location actual = currentData.Item1 + currentData.Item2;
                Assert.AreEqual(expected, actual, "Left<" + currentData.Item1 + ">. Right<" + currentData.Item2 + ">");
            }
        }

        /// <summary>
        ///A test for op_Subtraction
        ///</summary>
        [TestMethod()]
        public void op_SubtractionTest()
        {
            Tuple<Location, Location, Location>[] sampleData = { new Tuple<Location, Location, Location>(new Location(0, 0), new Location(1, 1), new Location(-1, -1)),
                                                                 new Tuple<Location, Location, Location>(new Location(0, 0), new Location(0, 0), new Location(0, 0)),
                                                                 new Tuple<Location, Location, Location>(new Location(1, 1), new Location(0, 0), new Location(1, 1)),
                                                                 new Tuple<Location, Location, Location>(new Location(1, -1), new Location(1, 1), new Location(0, -2)),
                                                                 new Tuple<Location, Location, Location>(new Location(100, 0), new Location(-50, 1), new Location(150, -1)),
                                                                 new Tuple<Location, Location, Location>(new Location(6, -5), new Location(1, 1), new Location(5, -6)),
                                                                 new Tuple<Location, Location, Location>(new Location(1, 1), new Location(1, 1), new Location(0, 0)),
                                                                 };
            foreach (Tuple<Location, Location, Location> currentData in sampleData)
            {
                Location expected = currentData.Item3;
                Location actual = currentData.Item1 - currentData.Item2;
                Assert.AreEqual(expected, actual, "Left<" + currentData.Item1 + ">. Right<" + currentData.Item2 + ">");
            }
        }

        /// <summary>
        ///A test for op_Subtraction
        ///</summary>
        [TestMethod()]
        public void op_Equal_not_equal()
        {
            Tuple<Location, Location, bool>[] sampleData = { new Tuple<Location, Location, bool>(new Location(0, 0), new Location(0, 0), true),
                                                                 new Tuple<Location, Location, bool>(new Location(0, 1), new Location(0, 0), false),
                                                                 new Tuple<Location, Location, bool>(new Location(1, 0), new Location(0, 0), false),
                                                                 new Tuple<Location, Location, bool>(new Location(1, 1), new Location(0, 0), false),
                                                                 new Tuple<Location, Location, bool>(new Location(0, 0), new Location(1, 0), false),
                                                                 new Tuple<Location, Location, bool>(new Location(0, 0), new Location(0, 1), false),
                                                                 new Tuple<Location, Location, bool>(null, new Location(0, 1), false),
                                                                 new Tuple<Location, Location, bool>(new Location(0, 0), null, false),
                                                                 new Tuple<Location, Location, bool>(null, null, true),
                                                                 };
            foreach (Tuple<Location, Location, bool> currentData in sampleData)
            {
                bool expected = currentData.Item3;
                bool actual = currentData.Item1 == currentData.Item2;
                Assert.AreEqual(expected, actual, "Equality: Left<" + currentData.Item1 + ">. Right<" + currentData.Item2 + ">");

                bool actualInequality = currentData.Item1 != currentData.Item2;
                Assert.AreEqual(!expected, actualInequality, "Inequality: Left<" + currentData.Item1 + ">. Right<" + currentData.Item2 + ">");
            }
        }

        /// <summary>
        ///A test for X
        ///</summary>
        [TestMethod()]
        public void XTest()
        {
            Tuple<Location, int>[] sampleData = { new Tuple<Location, int>(new Location(0, 0), 0),
                                                  new Tuple<Location, int>(new Location(1, 0), 1),
                                                  new Tuple<Location, int>(new Location(0, 1), 0),
                                                  new Tuple<Location, int>(new Location(10, 0), 10),
                                                  new Tuple<Location, int>(new Location(0, 10), 0),
                                                  new Tuple<Location, int>(new Location(10, 10), 10),
                                                  };
            foreach (Tuple<Location, int> currentData in sampleData)
            {
                int expected = currentData.Item2;
                int actual = currentData.Item1.X;
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///A test for Y
        ///</summary>
        [TestMethod()]
        public void YTest()
        {
            Tuple<Location, int>[] sampleData = { new Tuple<Location, int>(new Location(0, 0), 0),
                                                  new Tuple<Location, int>(new Location(1, 0), 0),
                                                  new Tuple<Location, int>(new Location(0, 1), 1),
                                                  new Tuple<Location, int>(new Location(10, 0), 0),
                                                  new Tuple<Location, int>(new Location(0, 10), 10),
                                                  new Tuple<Location, int>(new Location(10, 10), 10),
                                                  };
            foreach (Tuple<Location, int> currentData in sampleData)
            {
                int expected = currentData.Item2;
                int actual = currentData.Item1.Y;
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">");
            }
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes
    }
}